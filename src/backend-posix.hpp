#include "nn-backend.h"
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>

namespace NetioBackend
{
    struct posix_lsocket: public nn_listen_socket{
        //int fd;
        explicit posix_lsocket(int lsocket) : nn_listen_socket(lsocket){}
        bool operator<(const posix_lsocket& rhs) const
        {
            if (fd < rhs.fd)
            {
            return true;
            }
            return false;
        }
        bool operator==(const posix_lsocket& rhs) const
        {
            if (fd == rhs.fd)
            {
            return true;
            }
            return false;
        }
    };


    struct posix_ssocket: public nn_send_socket{
        //int fd;
        std::mutex send_mutex;
        network_buffer* buf;
        socket_addr remote_addr;
        posix_ssocket(int ssocket, const socket_addr& addr) : nn_send_socket(ssocket), buf(NULL), remote_addr(addr){}
        posix_ssocket() : nn_send_socket(-1), buf(NULL), remote_addr({"", 0}){}
        bool operator<(const posix_ssocket& rhs) const
        {
            if (fd < rhs.fd)
            {
            return true;
            }
            return false;
        }
        bool operator==(const posix_ssocket& rhs) const
        {
            if (fd == rhs.fd)
            {
            return true;
            }
            return false;
        }
    };



    class BackendPosix: public NetworkBackend {
    public:
        explicit BackendPosix(network_config& config, Eventloop* evloop);
        ~BackendPosix();
        void run() override;
        void register_handle(network_handle* handle) override;
        void close_handle(network_handle* handle) override;
        size_t send_data(const char* data, size_t size, send_handle& handle, bool data_in_mr = false) override;
    private:
        Eventloop* m_evloop;
        std::mutex m_mux;
        size_t m_buffersize;
        std::vector<posix_lsocket> m_lsockets;
        std::vector<send_handle> m_closing_handles;
        std::map<socket_addr, posix_ssocket> m_connection_map;
        std::map<int, network_buffer> m_buf_map;
        //Callbacks
        std::function<void(const network_handle& info, char* data, int size, NetworkBackend* backend, void* usr_ctx)> m_on_data_cb;
        std::function<void(const network_handle& info,NetworkBackend* backend, void* usr_ctx)> m_on_connection_established_cb;
        std::function<void(const network_handle& info,NetworkBackend* backend, void* usr_ctx)> m_on_connection_closed_cb;
        std::function<void(const network_handle& info,NetworkBackend* backend, void* usr_ctx)> m_on_connection_refused_cb;
        std::function<void(const network_handle& info,NetworkBackend* backend, void* usr_ctx)> m_on_send_completed_cb;

        int createListenSocket(listen_handle* lhandle);
        void check_recv_return(int ret);
        void send_connect(send_handle& handle);
        void close_handle_ev(int fd, const void* data);
        void on_connection_request(int lsocket, void* data);
        void on_connection_closed(int socket, void* data);
        void on_data(int socket, void* data);
    };
};
