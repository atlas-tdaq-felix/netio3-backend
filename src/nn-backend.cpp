#include "nn-backend.h"
#include "backend-libfabric.hpp"
#include "backend-posix.hpp"
#include "backend-ucx.hpp"

namespace NetioBackend{
    std::unique_ptr<NetworkBackend> NetworkBackend::create(network_type type, network_config& config, Eventloop* evloop){
        if (type == LIBFABRIC){
            return std::make_unique<BackendLibfabric>(config, evloop);
        } else if (type == UCX) {
            return std::make_unique<BackendUCX>(config, evloop);
        } else if (type == POSIX_SOCKETS){
            return std::make_unique<BackendPosix>(config, evloop);
        } else {
            log_error("Error: unknown network type. Using posix sockets as fallback.");
            return std::make_unique<BackendPosix>(config, evloop);
        }
    }
}