#include "backend-posix.hpp"

namespace NetioBackend{

BackendPosix::BackendPosix(network_config& config, Eventloop* evloop) :    NetworkBackend(config),
                                                        m_evloop(evloop),
                                                        m_buffersize(config.buffersize),
                                                        m_on_data_cb(config.on_data_cb),
                                                        m_on_connection_established_cb(config.on_connection_established_cb),
                                                        m_on_connection_closed_cb(config.on_connection_closed_cb),
                                                        m_on_connection_refused_cb(config.on_connection_refused_cb),
                                                        m_on_send_completed_cb(config.on_send_completed_cb)

{
    log_set_level(LOG_INFO);
    if (config.mode != TCP){
        // log_error("Posix sockets only support TCP. Continue with using TCP.");
        throw std::invalid_argument("Posix sockets only support TCP.");
    }
}

BackendPosix::~BackendPosix(){
    std::for_each(m_lsockets.begin(), m_lsockets.end(), [=](const auto& lsocket){m_evloop->remove_fd(lsocket.fd);});
    std::for_each(m_connection_map.begin(), m_connection_map.end(), [=](const auto& connection){m_evloop->remove_fd(connection.second.fd);});
}

/**
 * @brief This function creates a new listen socket.
 * It registers the FD with the eventloop so that the on_connection_request callback is triggered, when there is a conneciton request
 * and the socket can be used in a non-blocking way.
 *
 * @param lhandle: listen_handle struct that contains the ip and port the new socket listens on
 */
int BackendPosix::createListenSocket(listen_handle* lhandle){
    int listensocket = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if (listensocket == -1)
    {
        log_error("Cannot create TCP listen socket");
        return -1;
    }
    struct sockaddr_in local_addr;
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(lhandle->local_addr.port);
    inet_pton(AF_INET, lhandle->local_addr.ip.c_str(), &local_addr.sin_addr);
    if (bind(listensocket, (struct sockaddr *)&local_addr, sizeof(local_addr)) == -1)
    {
        log_error("Can't bind to IP/port");
        return -1;
    }

    if (listen(listensocket, SOMAXCONN) == -1)
    {
        log_error("Can't listen !");
        return -1;
    }
    std::cout << "Listening at address: " << inet_ntoa(local_addr.sin_addr) << " and port: " << ntohs(local_addr.sin_port) << std::endl;
    m_lsockets.emplace_back(listensocket);
    lhandle->lsocket = &m_lsockets.back();
    auto ctx = ev_context{m_lsockets.back().fd, lhandle->lsocket, std::bind(&BackendPosix::on_connection_request, this, std::placeholders::_1, std::placeholders::_2)};
    m_evloop->register_fd(ctx);
    return listensocket;
}

/**
 * @brief Callback to handle connection requests for a given lsiten socket.
 *
 * @param lsocket: FD of listen socket
 * @param data: not used in this callback
 */
void BackendPosix::on_connection_request(int lsocket, void* data){
    sockaddr_in remote;
    socklen_t remoteSize = sizeof(remote);
    int socket = accept(lsocket, (struct sockaddr *)&remote, &remoteSize);
    //posix_ssocket ssocket(socket, {inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)});
    if (socket == -1)
    {
        log_error("Can't connect with remote");
        if(m_on_connection_refused_cb != NULL){
            m_on_connection_refused_cb(send_handle{{inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)}, connection_parameters(), NULL}, this, m_config.usr_ctx);
        }
        return;
    }
    {
        std::unique_lock<std::mutex> lck(m_mux);
        m_buf_map[socket].pos = 0;
        m_buf_map[socket].left_to_read = 0;
        m_buf_map[socket].size = m_buffersize;
        m_buf_map[socket].buf_array.resize(m_buffersize, '\0');
        m_buf_map[socket].buf = m_buf_map[socket].buf_array.data();
        //m_connection_map[socket_addr{inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)}] = posix_ssocket(socket, {inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)});
        auto addr = socket_addr{inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)};
        m_connection_map.emplace(std::piecewise_construct, std::forward_as_tuple(socket_addr{inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)}),
                                                            std::forward_as_tuple(socket, addr));
        m_connection_map.at(socket_addr{inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)}).buf = &m_buf_map[socket];
    }
    std::cout << "Remote address: " << inet_ntoa(remote.sin_addr) << " and port: " << ntohs(remote.sin_port) << std::endl;
    auto ctx = ev_context{socket, &m_connection_map.at(socket_addr{inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)}), std::bind(&BackendPosix::on_data, this, std::placeholders::_1, std::placeholders::_2), std::bind(&BackendPosix::on_connection_closed, this, std::placeholders::_1, std::placeholders::_2)};
    m_evloop->register_fd(ctx);

    if(m_on_connection_established_cb != NULL){ //data contains a pointer to the posix_lsocket
        m_on_connection_established_cb(listen_handle{{"", 0}, connection_parameters(), static_cast<nn_listen_socket*>(data), {{inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)}, connection_parameters(), NULL}}, this, m_config.usr_ctx);
    }
}


/**
 * @brief Callback to notify the backend and the user that a connection was closed.
 *
 * @param socket: FD of affected socket
 * @param data: not used in this callback
 */
void BackendPosix::on_connection_closed(int socket, void* data){
    log_info("on_connection_closed: %d", socket);
    auto pos = std::find_if(m_connection_map.begin(), m_connection_map.end(), [=](const auto& connection){return &connection.second == data;});
    if(pos != m_connection_map.end()){ //need to check manually, since std::map::erase() does not take map.end()
        send_handle handle{{pos->first.ip, pos->first.port}, connection_parameters(), static_cast<nn_send_socket*>(data)};//{"", 0},{"", 0}, -1, socket};
        if(m_on_connection_closed_cb != NULL){
            m_on_connection_closed_cb(handle, this, m_config.usr_ctx);
        }
        std::unique_lock<std::mutex> lck(m_mux);
        m_connection_map.erase(pos);
    }
}

void BackendPosix::check_recv_return(int ret){
    if (ret == -1){
        log_error("Connection issue observed.");
    } else if (ret < 0){
        log_error("Unexpected problem.");
        char buffer[256];
        std::cout << "Recv returned: " << ret;
        char * errMsg = strerror_r(errno, buffer, 256);
        std::cout << ": " << errMsg << std::endl;
    }
}

/**
 * @brief Callback for new available data
 *
 * First, the data is received and written into the previously allocated buffer for this socket.
 * The function should take care that the received data is correctly distributed in messages.
 * If there is still data to receive from the previous message, this is done first to preserve the message order.
 *
 * @param socket: FD of receiving socket
 * @param data: not used in this callback
 */
void BackendPosix::on_data(int socket, void* data){
    auto ssocket = reinterpret_cast<posix_ssocket*>(data);
    auto buf_data = ssocket->buf;
    if(buf_data == NULL){
        log_error("No buffer associated to this socket: %d.", socket);
        return;
    }
    char* buf = buf_data->buf + buf_data->pos;
    ssize_t bytes_recv_err = recv(ssocket->fd, buf, buf_data->size - buf_data->pos, MSG_DONTWAIT);
    if (bytes_recv_err <= 0) {
        check_recv_return(bytes_recv_err);
        buf_data->pos = 0;
        buf_data->left_to_read = 0;
        return;
    }
    size_t bytes_recv = bytes_recv_err + buf_data->pos;
    log_debug("%d bytes read (including leftover form previous read)(size: %d, position: %d) \n", bytes_recv, buf_data->size, buf_data->pos);
    size_t current_pos = 0;
    while (current_pos < bytes_recv){
        log_debug("Entering while loop for new iteration. Starting at: %d, total bytes received: %d", current_pos, bytes_recv);
        if ((bytes_recv - current_pos) < sizeof(size_t)){
            std::copy(buf_data->buf + current_pos, buf_data->buf + bytes_recv, buf_data->buf);
            buf_data->left_to_read = sizeof(size_t) - bytes_recv;
            buf_data->pos = bytes_recv;
            return;
        }
        size_t msg_size = std::strtol(buf_data->buf + current_pos, NULL, 10);
        if ((bytes_recv - current_pos - sizeof(size_t)) < msg_size){
            if(current_pos > 0){
                std::copy(buf_data->buf + current_pos, buf_data->buf + bytes_recv, buf_data->buf);
            }

            buf_data->left_to_read = sizeof(size_t) - bytes_recv;
            buf_data->pos = bytes_recv - current_pos;
            return;
        } else {
            m_config.on_data_cb(send_handle{{"", 0}, connection_parameters(), ssocket}, buf_data->buf + current_pos + sizeof(size_t), msg_size, this, m_config.usr_ctx);
        }
        current_pos += sizeof(size_t) + msg_size;
    }
    buf_data->pos = 0;
    buf_data->left_to_read = 0;
    std::memset(buf_data->buf, '\0', m_buffersize);
    return;
}

void BackendPosix::run(){
    m_evloop->evloop_run();
}

/**
 * @brief Function to register a network handle
 *
 * The function takes a generic network handle of type std::variant.
 * For the posix backend this handle is to be expected to be of type send_handle or listen_handle.
 * If the handle specifies a local ip address and port, a listen socket will be created that listens and accpet incoming connections at this addess.
 * When a remote address is specified, a new socket will be created, which attempts to connect to the remote endpoint.
 *
 * TODO: Right now this implementation relies on the fact that the buffer size is configured correctly at the initialization of the backend object.
 * In praxis, the buffer size is determined by the bus on the client side. So we need a way to pass the buffer size when establishing a new connection.
 *
 * @param handle: generic network handle. For this backend it should be of type listen_handle or send_handle.
 */
void BackendPosix::register_handle(network_handle* handle){
    std::visit(
    overloaded{[=](listen_handle& handle) {
            // listen handle means we need to create a new listen socket and wait for incoming connections
            if (handle.local_addr.ip != "") {
                if ( createListenSocket(&handle) < 0){
                    throw std::runtime_error("Could not establish listen socket");
                }
            } else {
                throw std::invalid_argument("listen handle does not contain IP.");
            }
        },
        [=](send_handle& handle){
            // send handle means we need to establish a new connection to a remote endpoint
            if(handle.remote_addr.ip != "") {
                try{
                    send_connect(handle);
                } catch (const std::runtime_error& e){
                    throw; 
                } 
            } else {
                throw std::invalid_argument("send handle does not contain remote IP.");
            }
        },
        [](std::monostate& handle) {
            throw std::invalid_argument("Network handle is not initialized");
        },
        [](auto& handle) {
            throw std::invalid_argument("Wrong handle for this network provider. Expecting TCP handle.");
        },
    }, *handle);
}


/**
 * @brief Callback closing a network handle
 *
 * Using a callback to close a connection ensures that all outstanding send and recv operations are executed before the connection is closed since all operations are serialized by the event loop.
 *
 * @param fd: not used in this callback
 * @param data: pointer to send_handle struct that determins which connection should be closed.
 */
void BackendPosix::close_handle_ev(int socket, const void* data){
    std::unique_lock<std::mutex> lck(m_mux);
    m_evloop->remove_fd(socket);
    auto pos = std::find_if(m_connection_map.begin(), m_connection_map.end(), [=](const auto& connection){return &connection.second == data;});
    if(pos != m_connection_map.end()){ //need to check manually, since std::map::erase() does not take map.end()
        m_connection_map.erase(pos);
    }
    m_buf_map.erase(socket);
}


/**
 * @brief Function to close a network handle
 *
 * If the listen socket is closed, this is done right away.
 * If a connection is about to be closed this is done via a signal throuth the event loop to ensure that all outstanding operations can be executed before the connection is actually closed.
 * This also ensures that there are no locks necessary in the on_data callback since the two functions are serialized by the eventloop.
 *
 * @param handle: generic network handle. For this backend it should be of type send_handle or listen_handle
 */
void BackendPosix::close_handle(network_handle* handle){
    std::visit(
    overloaded{[=](listen_handle& handle) {
            if (handle.lsocket == NULL){
                throw std::invalid_argument("Empty network handle.");
            }
            m_evloop->remove_fd(handle.lsocket->fd);
            m_lsockets.erase(std::remove(m_lsockets.begin(), m_lsockets.end(), *static_cast<posix_lsocket*>(handle.lsocket)), m_lsockets.end());
            handle.lsocket = NULL;
        },
        [=](send_handle& handle) {
            if (handle.ssocket == NULL){
                throw std::invalid_argument("Empty network handle.");
            }
            m_closing_handles.emplace_back(handle);
            auto close_signal = EvSignal(&m_closing_handles.back(),  std::bind(&BackendPosix::close_handle_ev, this, std::placeholders::_1, std::placeholders::_2), m_evloop, true, true);
            close_signal.fire();
            handle.ssocket = NULL;

        },
        [](std::monostate& handle) {
            throw std::invalid_argument("Network handle is not initialized");
        },
        [](auto& handle) {
            throw std::invalid_argument("Wrong handle for this network provider. Expecting TCP handle.");
        },
    }, *handle);
}


/**
 * @brief Function to estadblish a connection to remote endpoint
 *
 * The function creates a new socket and attempts to establish a connect it to a remote endpoint.
 * Potentially, this function will block until the connection is up unless there is a ECONNREFUSED error.
 *
 * After the connection is up, it also allocates a buffer to receive data on.
 *
 * @param handle: send handle that must contain the ip and port of the remote endpoint to connect to
 */
void BackendPosix::send_connect(send_handle& handle){
    auto sock = socket(AF_INET, (SOCK_STREAM | SOCK_NONBLOCK), 0);
    if (sock < 0){
        throw std::runtime_error("Cannot create socket.");
    }
    struct sockaddr_in remote_addr;
    remote_addr.sin_family = AF_INET;
    remote_addr.sin_port = htons(handle.remote_addr.port);
    inet_pton(AF_INET, handle.remote_addr.ip.c_str(), &remote_addr.sin_addr);
    bool try_connecting = true;
    while(try_connecting){
        int ret = connect(sock, (struct sockaddr*)&remote_addr, sizeof(remote_addr));
        if (ret < 0) {
            if (errno == EINPROGRESS /*|| errno == ECONNREFUSED*/){continue;}
            //std::cout << "Remote address: " << inet_ntoa(remote_addr.sin_addr) << " and port: " << ntohs(remote_addr.sin_port) << " socket: " << sock << std::endl;
            throw std::runtime_error("Could not establish connection to remote, Errno: " + std::to_string(errno));
        }
        try_connecting = false;
    }
    {
        std::unique_lock<std::mutex> lck(m_mux);
        m_connection_map.emplace(std::piecewise_construct, std::forward_as_tuple(handle.remote_addr),
                                                            std::forward_as_tuple(sock, handle.remote_addr));
        handle.ssocket = &m_connection_map.at(handle.remote_addr);
        m_buf_map[sock].pos = 0;
        m_buf_map[sock].left_to_read = 0;
        m_buf_map[sock].size = handle.connection_param.buf_size;
        m_buf_map[sock].buf_array.resize(handle.connection_param.buf_size, '\0');
        m_buf_map[sock].buf = m_buf_map[sock].buf_array.data();
        m_connection_map.at(handle.remote_addr).buf = &m_buf_map[sock];
    }
    if(m_on_connection_established_cb != NULL){
        m_on_connection_established_cb(send_handle{{inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port)}, handle.connection_param, handle.ssocket}, this, m_config.usr_ctx);
    }
    auto ctx = ev_context{sock, handle.ssocket, std::bind(&BackendPosix::on_data, this, std::placeholders::_1, std::placeholders::_2)};
    m_evloop->register_fd(ctx);
}

/**
 * @brief Function to send data to a remote endpoint
 *
 * Since we are using SOCK_STREAM, we need to add a header for each message that let the receiver know when one message finishes and the tnext one starts.
 * Additionially, the function will also try to establish a connection first if it is now up yet. Not sure if we want to change this or not.
 *
 * TODO: Similar to the recv function we need some sort of buffer is a message is not sent in one go. This buffer should then be sent first before attemoting to send the next message.
 *
 * @param data: pointer to the data that needs to be sent
 * @param size: amount of data that is sent
 * @param handle: send_handle
 */
size_t BackendPosix::send_data(const char* data, size_t size, send_handle& handle, bool data_in_mr){
    char header[sizeof(size_t)];
    int ret = 0;
    snprintf(header, sizeof(header), "%zu", size);
    if(m_connection_map.count(handle.remote_addr) > 0){
        std::unique_lock<std::mutex> lck(m_connection_map.at(handle.remote_addr).send_mutex); //Need to lock here to ensure message integrity in connection stream
        ret = send(m_connection_map.at(handle.remote_addr).fd, header, sizeof(size_t), 0);
        if(ret == sizeof(size_t)){
            ret = send(m_connection_map.at(handle.remote_addr).fd, data, size, 0);
        } else {
            return -1;
        }
    } else if (handle.ssocket != NULL && std::find_if(m_connection_map.begin(), m_connection_map.end(), [=](auto& connection){return &connection.second == handle.ssocket ;}) != m_connection_map.end()) {
        auto ssocket = static_cast<posix_ssocket*>(handle.ssocket);
        std::unique_lock<std::mutex> lck(ssocket->send_mutex);
        ret = send(ssocket->fd, header, sizeof(size_t), 0);
        if(ret == sizeof(size_t)){
            ret = send(ssocket->fd, data, size, 0);
        } else {
            return -1;
        }
    } else {
        throw std::runtime_error("No socket for remote address. Have you establsihed the connection?");
    }
    if (ret != size){
        return -1;
    }
    if (m_on_send_completed_cb != NULL){
        m_on_send_completed_cb(handle, this, m_config.usr_ctx);
    }
    return 0;
}

} //namespace NetioBackend
