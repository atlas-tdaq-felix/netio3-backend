#!/usr/bin/env python3

import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestPingPong(FelixTestCase):

    def setUp(self):
        self.start('pong')

    def tearDown(self):
        self.stop('pong')

    def test_pingpong(self):
        try:
            timeout = 30
            messages = "15"
            netio_type = FelixTestCase.netio_type
            netio_mode = FelixTestCase.netio_mode
            port = FelixTestCase.port
            output = subprocess.check_output(' '.join(("./ping_pong", "--client", "--port", port, "--type", netio_type, "--mode", netio_mode, "--messages", messages)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
