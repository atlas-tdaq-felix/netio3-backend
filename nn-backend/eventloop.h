#pragma once

#include <cstring>
#include <iostream>
#include <array>
#include <map>
#include <sys/epoll.h>
#include <fcntl.h>
#include <functional>
#include <mutex>
#include <sys/eventfd.h>
#include <sys/timerfd.h>
#include <unistd.h>
#include <atomic>
extern "C" {
  #include "log.h"
}

#define MAX_EPOLL_EVENTS (128)
#define EPOLL_TIMEOUT (1000)

class Eventloop;

struct ev_context
{
    int fd = -1;
    void* data = NULL;
    std::function<void(int,void*)> cb;
    std::function<void(int,void*)> on_closed_cb;
};

class Eventloop {
  private:
    std::map<int, ev_context> m_ev_contexts_by_fd;
    int m_epollfd; // no flag passed, same behaviour as epoll_create
    std::array<struct epoll_event, MAX_EPOLL_EVENTS> m_events;
    std::function<void()> cb_init = NULL;
    std::vector<int> m_open_fds;
    std::vector<ev_context> m_ev_contexts;
    std::atomic_bool ev_should_stop = false;
    std::mutex m_fd_mutex;

    void process_event(struct ev_context* evc);
  public:
    explicit Eventloop(std::function<void()> cb_init = NULL);
    void evloop_run();
    void evloop_run_for(uint64_t t_sec);
    void register_fd(ev_context& ctx, bool no_sempahore = false); //rename function
    void remove_fd(int fd);
    void stop();
};

class EvSignal{
  private:
    void* m_data;
    std::function<void(int,void*)> m_cb;
    Eventloop* m_evloop;
    ev_context m_ev_ctx;
    void exec_once(int fd,void*){
        uint64_t buf;
        if(8 != read(fd, &buf, 8)) {
            log_info("Did not read 8 bytes.");
        }
        m_cb(m_ev_ctx.fd, m_data);
        m_evloop->remove_fd(m_ev_ctx.fd);
    }
    void exec(int fd,void*){
        uint64_t buf;
        if(8 != read(fd, &buf, 8)) {
            log_info("Did not read 8 bytes.");
        }
        m_cb(m_ev_ctx.fd, m_data);
    }
  public:  
    EvSignal(void* data, std::function<void(int,void*)> cb, Eventloop* evloop, bool use_semaphore, bool exec_once) :    m_data(data),
                                                                                                                        m_cb(cb),
                                                                                                                        m_evloop(evloop),
                                                                                                                        m_ev_ctx(ev_context())
    {
        if(use_semaphore){
            m_ev_ctx.fd = eventfd(0, EFD_NONBLOCK | EFD_SEMAPHORE);
        } else {
            m_ev_ctx.fd = eventfd(0, EFD_NONBLOCK);
        }
        if (exec_once){
            m_ev_ctx.cb =  std::bind(&EvSignal::exec_once, this, std::placeholders::_1, std::placeholders::_2);
        } else {
            m_ev_ctx.cb =  std::bind(&EvSignal::exec, this, std::placeholders::_1, std::placeholders::_2);
        }

        m_ev_ctx.data = this;
        if(m_ev_ctx.fd == -1)
        {
            log_fatal("Could not open eventfd");
            exit(2);
        }
        m_evloop->register_fd(m_ev_ctx, true);
    }

    ~EvSignal(){
        m_evloop->remove_fd(m_ev_ctx.fd);
    }

    void fire(){
        uint64_t buf = 1;
        int ret = write(m_ev_ctx.fd, &buf, 8);
        if( ret !=8 ){
            log_error("Firing signal writing on fd %d, only %d / 8 bytes written. Errno %s", m_ev_ctx.fd, ret, strerror(errno));
        }
    }

    void set_data(void* data){
        m_data = data;
    }
};

class EvTimer{
  private:
    ev_context timer_ctx;
    Eventloop* evloop;
    std::function<void(int,void*)> cb;

    void set_timerfd(int fd, unsigned long s, unsigned long ns)
    {   
        log_info("Starting timer with interval: %lu sec and %lu nsec", s, ns);
        itimerspec it;
        it.it_interval.tv_sec = s;
        it.it_interval.tv_nsec = ns;
        it.it_value.tv_sec = s;
        it.it_value.tv_nsec = ns;
        if(timerfd_settime(fd, 0, &it, NULL)) {
            log_error("Could not set timerfd %d. The timer will not fire.", fd);
        }
    }

    void timer_callback(int fd, void* data)
    {
        uint64_t buf;
        if(8 != read(fd, &buf, 8)) {
            log_debug("Did not read 8 bytes. Instead: %d",  buf);
        }
        if(cb){
            cb(timer_ctx.fd, timer_ctx.data);
        }
    }


  public:  
    explicit EvTimer(Eventloop* ev, std::function<void(int,void*)> cb) :    evloop(ev),
                                                                            cb(cb)
    {
        timer_ctx.fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
        timer_ctx.data = this;
        timer_ctx.cb = std::bind(&EvTimer::timer_callback, this, std::placeholders::_1, std::placeholders::_2);;
        evloop->register_fd(timer_ctx, true);
    }
    ~EvTimer(){
        evloop->remove_fd(timer_ctx.fd);
    }
    void start_s(uint64_t t){start_us(t * 1000 * 1000);}
    void start_ms(uint64_t t){start_us(t * 1000);}
    void start_us(uint64_t t){
        set_timerfd(timer_ctx.fd, t/(1000*1000), (t%(1000*1000))*1000);
    }
    void stop(){
        set_timerfd(timer_ctx.fd, 0, 0);
    }
};
