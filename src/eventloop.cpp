#include "eventloop.h"
#include <algorithm>

Eventloop::Eventloop(std::function<void()> cb_init) : m_epollfd(epoll_create1(0)),
                                                      cb_init(cb_init)
{
    if(m_epollfd == -1) {
      log_fatal("Could not create epoll fd. Exit.");
      exit(2);
    }
}


void Eventloop::process_event(struct ev_context* evc)
{
  if(evc->cb != NULL) {
      evc->cb(evc->fd, evc->data);
  }
}

void Eventloop::stop(){
  log_info("Stopping evloop.");
  ev_should_stop = true;
}


void Eventloop::register_fd(ev_context& ctx, bool no_sempahore){
    std::unique_lock<std::mutex> lck(m_fd_mutex);
    struct epoll_event ev;
    int ev_fd = ctx.fd;
    m_ev_contexts_by_fd[ev_fd] = ctx;
    ev.events = (EPOLLIN | EPOLLRDHUP);
    ev.data.ptr = &m_ev_contexts_by_fd[ev_fd];
    int rc = 0;
    if (no_sempahore) {
      rc = fcntl(ev_fd, F_SETFL, fcntl(ev_fd, F_GETFL) | O_NONBLOCK);
    } else {
      rc = fcntl(ev_fd, F_SETFL, fcntl(ev_fd, F_GETFL) | O_NONBLOCK | EFD_SEMAPHORE);
    }
    if (rc < 0) {
      log_error("Failed to change flags (incl. O_NONBLOCK) of file descriptor %d.", ev_fd);
    }
    log_debug("Adding %d to epoll %d", ev_fd, m_epollfd);
    if(epoll_ctl(m_epollfd, EPOLL_CTL_ADD, ev_fd, &ev))
    {
        log_error("Could not add file descriptor %d to epoll. Events from this resource will be neglected.", m_epollfd);
        return;
    }
    m_open_fds.push_back(ctx.fd);
}

void Eventloop::remove_fd(int fd){
  std::unique_lock<std::mutex> lck(m_fd_mutex);
  if (std::find(m_open_fds.begin(), m_open_fds.end(), fd) != m_open_fds.end()){
    close(fd);
    epoll_ctl(m_epollfd, EPOLL_CTL_DEL, fd, NULL);
    m_ev_contexts_by_fd.erase(fd);
    m_open_fds.erase(std::remove(m_open_fds.begin(), m_open_fds.end(), fd), m_open_fds.end());
  } else {
    log_warn("Tried to close FD %d which is not open. Doing nothing.", fd);
  }

}

void Eventloop::evloop_run_for(uint64_t t_sec){
  //start timer to stop evloop (ev_should_stop)

  EvTimer timer(this, [&](int, void*){this->stop();});
  timer.start_s(t_sec);
  evloop_run();
}



void Eventloop::evloop_run(){
  if(cb_init != NULL) {
      cb_init();
  }
  bool running = true;
  while(running) {
    // don't want to block or wait too long if we're shutting down
    uint64_t timeout = ev_should_stop ? 10 : EPOLL_TIMEOUT;
    int nevents = epoll_wait(m_epollfd, &m_events[0], MAX_EPOLL_EVENTS, timeout);
    log_trace("epoll wait: %d events to process", nevents);

    for(int i = 0; i < nevents; ++i)
    {
      //log_trace("event type: %x from fd %d", m_events[i].events, ((ev_context*)m_events[i].data.ptr)->fd);
      process_event(static_cast<ev_context*>(m_events[i].data.ptr));
      if(m_events[i].events & EPOLLRDHUP)
      {
        ev_context* c = (struct ev_context*)(m_events[i].data.ptr);
        if (c->on_closed_cb != NULL){
          c->on_closed_cb(c->fd, c->data);
        }
        remove_fd(c->fd);
      }
    }
    if (ev_should_stop.load() && nevents < 1) {
       running = false;
       continue;
    }

    if(nevents == -1)
    {
      int errsv = errno;
      if(errsv==EINTR) {
        continue;
      }
      else {
        log_fatal("Eventloop: non-blocking epoll_wait returned -1: %s", strerror(errsv));
        exit(1);
      }
    }
  }//end of while running
  std::unique_lock<std::mutex> lck(m_fd_mutex);
  close(m_epollfd);
  std::for_each(m_open_fds.begin(), m_open_fds.end(), [](auto& fd){close(fd);});
}


