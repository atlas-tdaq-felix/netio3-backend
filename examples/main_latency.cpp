#include <memory>
#include <thread>
#include <cmath>

#include "docopt/docopt.h"
#include "felixtag.h"
#include "eventloop.h"
#include "nn-backend.h"

static const char USAGE[] =
R"(latency-test - Latency test application (client and server).

    Usage:
      latency-test [options]

    Options:
      -h, --help                         Show this screen.
          --version                      Show version.
      -c, --client                       Run client
      -s, --server                       Run server
      -i, --ip=IP                        Specify local ip address [default: 127.0.0.1]
      -p, --port=PORT                    Specify local port [default: 1337]
      --remote-ip=rIP                     Specify local ip address [default: 127.0.0.1]
      --remote-port=rPORT                 Specify local port [default: 7331]
      -m, --mode=NETWORK_MODE            Specify network mode [default: TCP]
      -t, --type=NETWORK_TYPE            Specify network type [default: POSIX_SOCKETS]
          --messages=MESSAGES            Specify number of messages after which to exit [default: 0]
          --size=MESSAGE_SIZE            Specify message size for test [default: 4] 

    Network Mode: TCP, UDP, RDMA, SHMEM
    Network Type: NONE, POSIX_SOCKETS, LIBFABRIC, UCX
)";


struct {
    struct timespec t0;
    uint64_t messages_received;
    uint64_t bytes_received;
    std::vector<uint64_t> latencies;
} statistics;

struct latency_usr_ctx {
  unsigned int n = 0;
  unsigned int connections = 0;
  unsigned int messages = 0;
  unsigned int msg_size = 4;
  bool is_server;
  NetioBackend::send_handle* shandle; 
};

void on_data_server(const NetioBackend::network_handle& hdl, char* data, int size, NetioBackend::NetworkBackend* backend, void * ctx){
    latency_usr_ctx* usr = static_cast<latency_usr_ctx*>(ctx);
    try{
      int res = backend->send_data(data, size, *usr->shandle);
    } catch (const std::exception& e){
      std::cout << e.what() <<std::endl;
    }
}

void on_data_client(const NetioBackend::network_handle& hdl, char* data, int size, NetioBackend::NetworkBackend* backend, void * ctx){
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
    statistics.latencies.push_back(1e9*(t1.tv_sec - statistics.t0.tv_sec) + t1.tv_nsec - statistics.t0.tv_nsec);
    latency_usr_ctx* usr = static_cast<latency_usr_ctx*>(ctx);
    usr->n++;
    if (!usr->is_server && (usr->n >= usr->messages)) {
      std::cout << "CLIENT: All messages received" << std::endl;
      uint64_t avg = 0;
      uint64_t rms = 0;
      uint64_t N = 0;
      uint64_t largest = 0;
      for(unsigned int i=1; i<statistics.latencies.size(); ++i){
            if (statistics.latencies.at(i)>0 && statistics.latencies.at(i) < 1540000){
                if (statistics.latencies.at(i) > largest){largest = statistics.latencies.at(i);} 
                avg+=statistics.latencies.at(i);
                rms+=statistics.latencies.at(i)*statistics.latencies.at(i);
                N++;
            } else {
                std::cout<<"Time error discovered at"<<statistics.latencies.at(i)<<std::endl;
            }
      }
      avg/=N;
      rms = sqrt(rms)/(N-1);

      std::cout << "\nAveraged latency on "<< N << " measurements: " << avg << " +/- " << rms << " ns. With the longest latency of " << largest << " ns.\n"<< std::endl;
        
      exit(0); //TODO: no exit here
    }
    try{
      clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0); 
      int res = backend->send_data(data, size, *usr->shandle);
    } catch (const std::exception& e){
      std::cout << e.what() <<std::endl;
    }
}

void on_connection_established(const NetioBackend::network_handle& hdl, NetioBackend::NetworkBackend* backend, void * ctx){
    std::cout << "Latency test on connection established" << std::endl;
    latency_usr_ctx* usr = static_cast<latency_usr_ctx*>(ctx);
    if(!usr->is_server){
        usr->connections++;
        if(usr->connections > 1){
            char teststring[4096] = "";
            clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0); 
            int ret = backend->send_data(teststring, usr->msg_size, *usr->shandle);
            std::cout << "Initial send returned: " << ret << " last char " << std::endl;
        }
    } else {
      std::cout << "Registering server send handle" << std::endl;
      NetioBackend::network_handle shandle = *usr->shandle;
      backend->register_handle(&shandle);
    }
}

class LatencyTest{
    private:
        std::unique_ptr<NetioBackend::NetworkBackend> m_net_backend;
        NetioBackend::network_config m_config;
        bool m_server;
        latency_usr_ctx ctx;
        Eventloop m_evloop;
        std::thread m_evloop_thread;

    public:
        LatencyTest(NetioBackend::network_type networkType, const NetioBackend::network_config& config, bool is_server, std::string ip, short unsigned int port, std::string remote_ip, short unsigned int remote_port, unsigned messages=0, unsigned msg_size = 0) : m_config(config),
            m_server(is_server){
            ctx.n = 0;
            ctx.messages = messages;
            ctx.is_server = is_server;
            if(is_server){
              m_config.on_data_cb = on_data_server;
            } else {
              m_config.on_data_cb = on_data_client;
              msg_size < 4096 ? ctx.msg_size = msg_size : ctx.msg_size = 4096;
            }

            m_config.on_connection_established_cb = on_connection_established;
            m_config.usr_ctx = &ctx;

            m_net_backend = NetioBackend::NetworkBackend::create(networkType, m_config, &m_evloop);
            if(m_server){
                NetioBackend::network_handle shandle = NetioBackend::send_handle{{remote_ip, remote_port}, {m_config.mode, m_config.buffersize, m_config.num_buffers, false}, NULL};
                NetioBackend::network_handle lhandle = NetioBackend::listen_handle{{ip, port}, NetioBackend::connection_parameters(), NULL, NetioBackend::send_handle{}};
                ctx.shandle = &std::get<NetioBackend::send_handle>(shandle);
                m_evloop_thread = std::thread([=](){m_net_backend->run();});
                std::cout << "Registering server listen handle" << std::endl;
                m_net_backend->register_handle(&lhandle);
                while(true){
                sleep(1);
                }
            } else {
                NetioBackend::network_handle shandle = NetioBackend::send_handle{{remote_ip, remote_port}, {m_config.mode, m_config.buffersize, m_config.num_buffers, false}, NULL};
                NetioBackend::network_handle lhandle = NetioBackend::listen_handle{{ip, port}, NetioBackend::connection_parameters(), NULL, NetioBackend::send_handle{}};
                ctx.shandle = &std::get<NetioBackend::send_handle>(shandle);
                m_evloop_thread = std::thread([=](){m_net_backend->run();});
                std::cout << "Registering client listen handle" << std::endl;
                m_net_backend->register_handle(&lhandle);
                sleep(3);
                std::cout << "Registering client send handle" << std::endl;
                m_net_backend->register_handle(&shandle);
                std::cout << std::endl;
                while(true){
                    sleep(1);
                }
            }
        }
        ~LatencyTest(){
            m_evloop_thread.join();
        }
};

int main(int argc, char** argv){
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    try {
      std::string ip = args["--ip"].asString();
      short unsigned int port = args["--port"].asLong();
      std::string r_ip = args["--remote-ip"].asString();
      short unsigned int r_port = args["--remote-port"].asLong();
      bool run_server = args["--server"].asBool();
      bool run_client_and_server = !run_server && !args["--client"].asBool();
      NetioBackend::network_mode mode = NetioBackend::network_mode_from_string(args["--mode"].asString());
      NetioBackend::network_type type = NetioBackend::network_type_from_string(args["--type"].asString());
      unsigned messages = args["--messages"].asLong();
      unsigned size     = args["--size"].asLong();


      NetioBackend::network_config config{false, 12, 1024, mode, NULL, NULL, NULL, NULL, NULL};

      if (run_client_and_server) {
        std::thread server([=](){LatencyTest(type, config, true, ip, port, r_ip, r_port, messages);});
        sleep(1);
        auto client = LatencyTest(type, config, false, ip, port, r_ip, r_port, messages, size);
        server.join();
      } else {
        auto client_or_server = LatencyTest(type, config, run_server, ip, port, r_ip, r_port, messages, size);
        exit(0);
      }
    } catch (std::invalid_argument const& error) {
      std::cerr << "Argument or option of wrong type" << std::endl;
      std::cerr << error.what() << std::endl;
      std::cout << std::endl;
      std::cout << USAGE << std::endl;
      exit(-1);
    }

}