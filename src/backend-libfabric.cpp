#include <utility>

#include "backend-libfabric.hpp"

namespace NetioBackend{
void BackendLibfabric::setProvider(fi_fabric_attr* hints){
    if(m_config.mode == network_mode_value.at("RDMA")){
        hints->prov_name = strdup("verbs");
    } else if (m_config.mode == network_mode_value.at("TCP")){
        hints->prov_name = strdup("sockets");
    } else {
        log_error("Unknown network mode"); //throw exception
    }
}



void BackendLibfabric::setupDomains(const char* host, const char* port, uint64_t flags){
    fi_info* hints;
    hints = fi_allocinfo();
    hints->addr_format = FI_FORMAT_UNSPEC;
    hints->ep_attr->type = FI_EP_MSG;
    hints->caps = FI_MSG;
    hints->mode = FI_LOCAL_MR;
    setProvider(hints->fabric_attr);
    int ret = 0;
    
    log_debug("Registering Fabric and domains for %s:%s", host, port);
    if((ret = fi_getinfo(FI_VERSION(1, 1), host, port, flags, hints, &m_info)))
    {
        log_error("Failed to get info on local interface, error %d: %s", ret, fi_strerror(-ret));
        return;
    }
    fi_freeinfo(hints);
    //log_debug("Connected Endpoint with the following info: \n%s\n\n\n\n\n", fi_tostr(m_info, FI_TYPE_INFO));
    log_debug("addr format: %x, provider: %s", m_info->addr_format, m_info->fabric_attr->prov_name);
    if((ret = fi_fabric(m_info->fabric_attr, &m_fabric, NULL)))
    {
        log_error("Failed to open fabric, error %d: %s", ret, fi_strerror(-ret));
        return;
    }

    if((ret = fi_domain(m_fabric, m_info, &m_send_domain.domain_ptr, NULL)))
    {
        log_error("Cannot open send domain. Error ", ret, fi_strerror(-ret));
        return;
    }

    if((ret = fi_domain(m_fabric, m_info, &m_listen_domain.domain_ptr, NULL)))
    {
        log_error("Cannot open listen domain. Error ", ret, fi_strerror(-ret));
        return;
    }

    log_debug("Fabric and domains initialized.");

}

int BackendLibfabric::connect_endpoint(endpoint* ep, fid_domain* domain, fi_info* info){
    int ret=0;
    fi_eq_attr eq_attr;
    eq_attr.wait_obj = FI_WAIT_FD;

    if((ret = fi_eq_open(m_fabric, &eq_attr, &ep->eq, NULL)))
    {
        log_error("Failed to open Event Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
        return -1;
    }

    if((ret = fi_endpoint(domain, info, &ep->ep, NULL)))
    {
        log_error("Failed to open Endpoint for send socket, error %d: %s", ret, fi_strerror(-ret));
        return -1;
    }

    if((ret = fi_ep_bind(ep->ep, &ep->eq->fid, 0)))
    {
        log_error("Failed to bind endpoint, error %d: %s", ret, fi_strerror(-ret));
        return -1;
    }

    fi_cq_attr cq_attr;
    cq_attr.size = MAX_CQ_ENTRIES;      /* # entries for CQ */
    cq_attr.flags = 0;     /* operation flags */
    cq_attr.format = FI_CQ_FORMAT_DATA; //FI_CQ_FORMAT_CONTEXT;    /* completion format */
    cq_attr.wait_obj= FI_WAIT_FD;  /* requested wait object */
    cq_attr.signaling_vector = 0; /* interrupt affinity */
    cq_attr.wait_cond = FI_CQ_COND_NONE; /* wait condition format */ // The threshold indicates the number of entries that are to be queued before at the CQ before the wait is satisfied.
    cq_attr.wait_set = NULL;  /* optional wait set */


    if(domain == m_send_domain.domain_ptr){
        //FI_TRANSMIT CQ
        if((ret = fi_cq_open(domain, &cq_attr, &ep->cq, NULL)) != 0)
        {
            log_error("Failed to open Completion Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
            return -1;
        }

        if((ret = fi_ep_bind((ep->ep), &ep->cq->fid, FI_TRANSMIT)) != 0)
        {
            log_error("Failed to open Completion Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
            return -1;
        }
    } else {
        if((ret = fi_cq_open(domain, &cq_attr, &ep->rcq, NULL)) != 0)
        {
            log_error("Failed to open Completion Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
            return -1;
        }

        if((ret = fi_ep_bind((ep->ep), &ep->rcq->fid, FI_RECV)) != 0)
        {
            log_error("Failed to open Completion Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
            return -1;
        }
    }


    cq_attr.format = FI_CQ_FORMAT_UNSPEC;
    cq_attr.wait_obj= FI_WAIT_NONE;
    if(domain == m_send_domain.domain_ptr){
        //FI_RECV CQ - also necessary
        if((ret = fi_cq_open(domain, &cq_attr, &ep->rcq, NULL)) != 0)
        {
            log_error("Failed to open Completion Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
            return -1;
        }

        if((ret = fi_ep_bind((ep->ep), &ep->rcq->fid, FI_RECV)) != 0)
        {
            log_error("Failed to open Completion Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
            return -1;
        }
    } else {
        //FI_TRANSMIT CQ
        if((ret = fi_cq_open(domain, &cq_attr, &ep->cq, NULL)) != 0)
        {
            log_error("Failed to open Completion Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
            return -1;
        }

        if((ret = fi_ep_bind((ep->ep), &ep->cq->fid, FI_TRANSMIT)) != 0)
        {
            log_error("Failed to open Completion Queue for send socket, error %d: %s", ret, fi_strerror(-ret));
            return -1;
        }
    }


    if((ret = fi_enable(ep->ep)) != 0)
    {
        log_error("Failed to enable endpoint for send socket, error %d: %s", ret, fi_strerror(-ret));;
        return -1;
    }

    if((ret = fi_control(&ep->eq->fid, FI_GETWAIT, &ep->eqfd)) != 0)
    {
        log_error("Failed to retrive send socket Event Queue wait object, error %d: %s", ret, fi_strerror(-ret));
        return -1;
    }
    //log_debug("Connected Endpoint with the following info: \n%s\n\n\n\n\n", fi_tostr(info, FI_TYPE_INFO));
    return ret;
}

int BackendLibfabric::close_endpoint(endpoint* ep, fid_domain* domain){
    log_debug("Closing endpoint %p", ep);
    int ret = 0;

    if(ep->ep == NULL || (ret = fi_close(&ep->ep->fid)))
    {
        log_error("Failed to close send socket Endpoint %d: %s", ret, fi_strerror(-ret));
    }

    //EQ
    m_evloop->remove_fd(ep->eqfd);
    if(ep->eq == NULL || (ret = fi_close(&ep->eq->fid)))
    {
        log_error("Failed to close send socket Event Queue %d: %s", ret, fi_strerror(-ret));
    }

    //FI_TRANSMIT CQ
    m_evloop->remove_fd(ep->cqfd);
    if(ep->cq == NULL || (ret = fi_close(&ep->cq->fid)))
    {
        log_error("Failed to close send socket Completion Queue %d: %s", ret, fi_strerror(-ret));
    }
    //FI_RECV CQ
    if(ep->rcq == NULL || (ret = fi_close(&ep->rcq->fid)))
    {
        log_error("Failed to close FI_RECV send socket Completion Queue %d: %s", ret, fi_strerror(-ret));
    }

    if(ep->fi != NULL){
        fi_freeinfo(ep->fi);
        ep->fi = NULL;
    }
    // socket->state = DISCONNECTED;
    return ret;
}

void BackendLibfabric::init_send(send_handle* handle, fi_info* info){
    int ret=0;
    struct fi_info* hints;
    libfabric_ssocket* socket = static_cast<libfabric_ssocket*>(handle->ssocket);
    // Init infos

    hints = fi_allocinfo();
    if (info == NULL){
        hints->addr_format = FI_FORMAT_UNSPEC;
    } else {
        hints->addr_format = info->addr_format;
        hints->dest_addr = info->dest_addr;
        hints->dest_addrlen = info->dest_addrlen;
    }
    hints->ep_attr->type  = FI_EP_MSG;
    hints->caps = FI_MSG;
    hints->mode   = FI_LOCAL_MR;
    // As of libfabric 1.10, the tcp provider only support FI_PROGRESS_MANUAL
    // So the following will not allow the tcp provider to be used
    hints->domain_attr->data_progress = FI_PROGRESS_AUTO;
    hints->domain_attr->resource_mgmt = FI_RM_ENABLED;
    setProvider(hints->fabric_attr);
    uint64_t flags = 0;
    if (info == NULL){
        char port_addr[32];
        snprintf(port_addr, 32, "%u", handle->remote_addr.port);
        log_debug("Initializing send socket. Trying to send to : %s:%s", handle->remote_addr.ip.c_str(), port_addr);
        ret = fi_getinfo(FI_VERSION(1, 1), handle->remote_addr.ip.c_str(), port_addr, flags, hints, &socket->ep.fi);
    } else {
        ret = fi_getinfo(FI_VERSION(1, 1), NULL, NULL, flags, hints, &socket->ep.fi);
    }
    if(ret)
    {
        fi_freeinfo(hints);
        log_error("Failed to initialise socket, error %d: %s", ret, fi_strerror(-ret));
        return;
    }
    fi_freeinfo(hints);
    //Connect socket
    if(connect_endpoint(&socket->ep, m_send_domain.domain_ptr, socket->ep.fi)){
        //do something to try again
        log_warn("Connecting send ep failed.");
    }
    /* Connect to server */
    if((ret = fi_connect(socket->ep.ep, socket->ep.fi->dest_addr, NULL, 0)) != 0)
    {
        log_warn("Connection to remote failed, error %d: %s", ret, fi_strerror(-ret));
        return;
    }
    if((ret = fi_control(&socket->ep.eq->fid, FI_GETWAIT, &socket->ep.eqfd))){
        log_error("Cannot retrieve the Event Queue wait object of send socket, error %d: %s", ret, fi_strerror(-ret));
        return;
    }
    socket->eq_ev_ctx = ev_context{socket->ep.eqfd, socket, std::bind(&BackendLibfabric::on_send_socket_cm_event, this, std::placeholders::_1, std::placeholders::_2)};
    log_debug("EV context with FD: %d and data %p", socket->eq_ev_ctx.fd, socket->eq_ev_ctx.data);
    m_evloop->register_fd(socket->eq_ev_ctx);
    return;
}


void BackendLibfabric::init_listen(listen_handle* handle){
    int ret=0;
    struct fi_info* hints;
    struct fi_eq_attr eq_attr;
    eq_attr.wait_obj = FI_WAIT_FD;
    eq_attr.flags = 0;

    libfabric_lsocket* socket = static_cast<libfabric_lsocket*>(handle->lsocket);

    hints = fi_allocinfo();
    hints->addr_format = FI_FORMAT_UNSPEC;
    hints->ep_attr->type  = FI_EP_MSG;
    hints->caps = FI_MSG;
    hints->mode = FI_LOCAL_MR;
    setProvider(hints->fabric_attr);

    if((ret = fi_getinfo(FI_VERSION(1, 1), handle->local_addr.ip.c_str(), std::to_string(handle->local_addr.port).c_str(), FI_SOURCE, hints,
                        &socket->ep.fi)))
    {
        log_error("Failed to get info on local interface, error %d: %s", ret, fi_strerror(-ret));
        //ON_ERROR_BIND_REFUSED(socket, "fi_getinfo failed", ret);
        return;
    }
    fi_freeinfo(hints);
    log_debug("addr format: %x", socket->ep.fi->addr_format);

    if((ret = fi_eq_open(m_fabric, &eq_attr, &socket->ep.eq, NULL)))
    {
        log_error("Failed to open Event Queue for listen socket, error %d: %s", ret, fi_strerror(-ret));
        //ON_ERROR_BIND_REFUSED(socket, "fi_eq_open failed", ret);
        return;
    }

    if((ret = fi_passive_ep(m_fabric, socket->ep.fi, &socket->pep, NULL)))
    {
        log_error("Failed to open passive endpoint for listen socket, error %d: %s", ret, fi_strerror(-ret));
        //ON_ERROR_BIND_REFUSED(socket, "fi_passive_ep failed", ret);
        return;
    }

    if((ret = fi_pep_bind(socket->pep, &socket->ep.eq->fid, 0)))
    {
        log_error("Failed to bind passive endpoint for listen socket, error %d: %s", ret, fi_strerror(-ret));
        //ON_ERROR_BIND_REFUSED(socket, "fi_pep_bind failed", ret);
        return;
    }

    if((ret = fi_listen(socket->pep)))
    {
        log_error("Failed to enable listen socket, error %d: %s", ret, fi_strerror(-ret));
        //ON_ERROR_BIND_REFUSED(socket, "fi_listen failed", ret);
        return;
    }

    if((ret = fi_control(&socket->ep.eq->fid, FI_GETWAIT, &socket->ep.eqfd)))
    {
        log_error("Failed to retrive listen socket Event Queue wait object, error %d: %s", ret, fi_strerror(-ret));
        //ON_ERROR_BIND_REFUSED(socket, "fi_control failed", ret);
        return;
    }
    
    socket->eq_ev_ctx = ev_context{socket->ep.eqfd, socket, std::bind(&BackendLibfabric::on_listen_socket_cm_event, this, std::placeholders::_1, std::placeholders::_2)};
    
    m_evloop->register_fd(socket->eq_ev_ctx);
    log_debug("netio_listen_socket: registering EQ fd %d", socket->ep.eqfd);
    //log_debug("Connected Endpoint with the following info: \n%s\n\n\n\n\n", fi_tostr(socket->ep.fi, FI_TYPE_INFO));
}

int BackendLibfabric::read_cm_event(struct fid_eq* eq, struct fi_info** info, struct fi_eq_err_entry* err_entry){
    uint32_t event;
    struct fi_eq_cm_entry entry;

    ssize_t rd = fi_eq_sread(eq, &event, &entry, sizeof entry, 0, 0);
    if(rd < 0)
    {
        if(rd == -FI_EAGAIN)
        {
            return rd;
        }
        if(rd == -FI_EAVAIL)
        {
            int r;
            if((r = fi_eq_readerr(eq, err_entry, 0)) < 0)
            {
                log_error("Failed to retrieve details on Event Queue error", r);
            }
            log_error("Event Queue error: %s (code: %d), provider specific: %s (code: %d)",
                fi_strerror(err_entry->err), err_entry->err,
                fi_eq_strerror(eq, err_entry->prov_errno, err_entry->err_data, NULL, 0),
                err_entry->prov_errno);
            return rd;
        }
    }
    if (rd != sizeof entry)
    {
        log_error("Failed to read from Event Queue: %d", (int)rd);
    }

    if(info != NULL)
        *info = entry.info;

    return event;
}


// IMPORTANT: Contrary to the current implementation of netio-next, I try to use a single domain for all listen/recv sockets. This needs to be tests.
int BackendLibfabric::handle_connreq(libfabric_lsocket* lsocket, fi_info* info)
{
    // need to spawn new endpoint 
    log_debug("Received connection request.");
    recv_socket rsocket = recv_socket(this); //TODO: retreive information from info object
    if(connect_endpoint(&rsocket.ep, m_listen_domain.domain_ptr, info)){
        //do something to try again
        // TODO: clean up and throw error
        log_debug("Endpoint failed");
    }
    log_debug("Created and connected endpoint. Eqfd: %d", rsocket.ep.eqfd);
    int key = rsocket.ep.eqfd;
    // need to register callback for this 
    if(m_rsockets.count(key) > 0){
        log_error("Recv socket for this connection already exists.");
        return -1;
    }
    
    // send_handle handle = send_handle{{"", 0}, lsocket->conn_parameters, NULL};
    // m_ssockets.try_emplace(handle.remote_addr, handle, this);
    // handle.ssocket = &m_ssockets.at(handle.remote_addr);
    // m_ssockets.at(handle.remote_addr).addr = handle.remote_addr;
    // m_ssockets.at(handle.remote_addr).init_buffers();
    // init_send(&handle, info);
    
    if(m_rsockets.emplace(key, std::move(rsocket)).second){
        m_rsockets.at(key).init_buffers( m_config.num_buffers,m_config.buffersize);
        m_rsockets.at(key).eq_ev_ctx = ev_context{key, &m_rsockets.at(key), std::bind(&BackendLibfabric::on_recv_socket_cm_event, this, std::placeholders::_1, std::placeholders::_2)};
        m_evloop->register_fd(m_rsockets.at(key).eq_ev_ctx);
    
        int ret = 0;
        if(( ret = fi_accept( m_rsockets.at(key).ep.ep, NULL, 0)))
        {
            fi_reject(lsocket->pep, info->handle, NULL, 0);
            log_error("Listen socket, connection rejected, error %d: %s", ret, fi_strerror(-ret));
            throw std::runtime_error("Connection refused.");
        }
        log_debug("connection accepted. Lsocket EQ: %d, rsocket EQ %d", lsocket->fd,  m_rsockets.at(key).eq_ev_ctx.fd);
        return m_rsockets.at(key).ep.eqfd;
    } else {
        return -1;
    }

}



void BackendLibfabric::register_buffer(network_buffer* buf, int access_flag)
{
    domain_ctx* domain;
    if(access_flag == FI_RECV){
        domain = &m_listen_domain;
    } else if (access_flag == FI_SEND){
        domain = &m_send_domain;
    } else {
        log_fatal("Unknown flag for memory registration: %d", access_flag); 
        return;
    }
    buf->domain = domain;
    log_debug("Registering buffer of size %d for domain: %p", buf->size, domain);
    int ret = 0;
    if((ret = fi_mr_reg(domain->domain_ptr, buf->buf, buf->size, access_flag, 0, domain->req_key++, 0, &buf->mr, NULL)))
    {
        log_fatal("Failed to register buffer (key %lu) failed. Error %d: %s",domain->req_key, ret, fi_strerror(-ret));
    }

    if(ret==0){
        if (domain->reg_mr >= DOMAIN_MAX_MR){
            log_fatal("Trying to register recv buffer number %u. MR array size %lu.", domain->reg_mr, DOMAIN_MAX_MR);
        }
        log_debug("Current number of memory regions: %d", domain->reg_mr);
        //domain->mr.emplace_back(buf->mr->fid);
        domain->reg_mr++;
    }
}


void BackendLibfabric::post_buffers(network_buffer* buf, endpoint* ep)
{
    int ret=0;
    iovec iov{buf->buf, buf->size};
    void* desc;
    uint64_t flags;

    desc = fi_mr_desc(buf->mr);

    struct fi_msg msg;
    msg.msg_iov = &iov; /* scatter-gather array */
    msg.desc = &desc;
    msg.iov_count = 1;
    msg.addr = 0;
    msg.context = buf;
    msg.data = 0;

    flags = FI_REMOTE_CQ_DATA;//FI_MULTI_RECV;

    if((ret = fi_recvmsg(ep->ep, &msg, flags)) != 0)
    {
        log_error("Failed to post a buffer to receive inbound messages, error %d: %s", ret, fi_strerror(-ret));
    }

}


void BackendLibfabric::on_listen_socket_cm_event(int fd, void* ptr){
    auto lsocket = reinterpret_cast<libfabric_lsocket*>(ptr);
    log_debug("listen socket: connection event");
    fi_info *info = NULL;
    fi_eq_err_entry err_entry;
    int event = read_cm_event(lsocket->ep.eq, &info, &err_entry);

    switch (event)
    {
        case FI_CONNREQ:
            {
                log_debug("fi_verbs_process_listen_socket_cm_event: FI_CONNREQ");
                int rsocket_fd = handle_connreq(lsocket, info);
                log_debug("Connection done, allocating memory regions, fd: %d, number of buffers: %d", rsocket_fd, m_rsockets.at(rsocket_fd).buffers.size());
                if (m_rsockets.count(rsocket_fd) > 0){
                    int i = 0;
                    for(auto& buf : m_rsockets.at(rsocket_fd).buffers){
                        post_buffers(&buf, &m_rsockets.at(rsocket_fd).ep);
                        ++i;
                        log_info("Posted %d buffers of size %d to recv data", i, buf.size);
                    }
                }
            }
            break;

        case FI_CONNECTED:
            log_fatal("FI_CONNECTED received on listen socket");
            exit(2);

        case FI_SHUTDOWN:
            log_fatal("FI_SHUTDOWN received on listen socket");
            exit(2);

        case -FI_EAGAIN:
            // TODO: find new solution for new evloop class
            break;
            

        case -FI_EAVAIL:
            log_error("Unhandled error in listen socket EQ code: %d, provider specific code: %d",
                err_entry.err, err_entry.prov_errno);
            break;
    }
    //fi_freeinfo(info);
}


void BackendLibfabric::on_send_socket_cm_event(int fd, void* ptr){
    auto ssocket = reinterpret_cast<libfabric_ssocket*>(ptr);
    log_debug("send socket: connection event");
    //if(socket->state == DISCONNECTED){return;} TODO: check if still needed
    int ret = 0;
    struct fi_eq_err_entry err_entry;
    int event = read_cm_event(ssocket->ep.eq, NULL, &err_entry);

    switch(event)
    {
    case FI_SHUTDOWN:
        if (ssocket->ep.eqfd < 0 ){ //TODO: check if still necessary
            log_info("Ignoring FI_SHUTDOWN on send_socket, invalid eqfd (socket already closed)");
            return;
        }
        log_info("fi_verbs_process_send_socket_cm_event: FI_SHUTDOWN");
        if(m_on_connection_closed_cb != NULL){
            m_on_connection_closed_cb(send_handle{{ssocket->addr.ip, ssocket->addr.port}, ssocket->conn_parameters, ssocket}, this, m_config.usr_ctx);
        }
        m_ssockets.erase(ssocket->addr);
        m_send_domain.nb_sockets--;
        return;

    case FI_CONNECTED:
        if((ret = fi_control(&ssocket->ep.cq->fid, FI_GETWAIT, &ssocket->ep.cqfd)))
        {
            log_error("Failed to retrieve wait object for send socket Completion Queue", ret);
        }
        ssocket->cq_ev_ctx = ev_context{ssocket->ep.cqfd, ssocket, std::bind(&BackendLibfabric::on_send_socket_cq_event, this, std::placeholders::_1, std::placeholders::_2)};
        log_debug("send_socket: EQ fd %d connected, CQ fd %d", ssocket->ep.eqfd, ssocket->ep.cqfd);
        m_evloop->register_fd(ssocket->cq_ev_ctx);
        if(m_on_connection_established_cb != NULL){
            m_on_connection_established_cb(send_handle{{ssocket->addr.ip, ssocket->addr.port}, ssocket->conn_parameters, ssocket}, this, m_config.usr_ctx);
        }
        return;

    case FI_MR_COMPLETE:
    case FI_AV_COMPLETE:
    case FI_JOIN_COMPLETE:
        // Not implemented
        break;

    case -FI_EAVAIL:
        switch(err_entry.err) {
            case FI_ECONNREFUSED:
                log_debug("Connection refused (FI_ECONNREFUSED). Deallocating send_socket resources.");
                if(m_on_connection_refused_cb != NULL){
                    m_on_connection_refused_cb(send_handle{{ssocket->addr.ip, ssocket->addr.port}, ssocket->conn_parameters, ssocket}, this, m_config.usr_ctx);
                }
                m_ssockets.erase(ssocket->addr);
            case FI_ETIMEDOUT:
                log_info("fi_verbs_process_send_socket_cm_event: FI_ETIMEDOUT");
                // if (socket->eqfd < 0 ){
                //     log_info("Ignoring FI_SHUTDOWN on send_socket, invalid eqfd (socket already closed)");
                //     break;
                // }

                // // Need to take care of receive socket as well

                // if (socket->recv_socket != NULL){
                //     if (socket->recv_socket->eqfd < 0 ){
                //         log_info("Ignoring FI_ETIMEDOUT on recv_socket, invalid eqfd (socket already closed)");
                //         return;
                //     }
                //     log_info("Shutting down receive socket on FI_ETIMEDOUT");
                //     handle_recv_socket_shutdown(socket->recv_socket);
                //     if(socket->recv_socket->lsocket->cb_connection_closed) {
                //         socket->recv_socket->lsocket->cb_connection_closed(socket->recv_socket);
                //     }
                // }

                // if(socket->cqfd < 0){ //cq not initalized yet
                //     handle_send_socket_shutdown_on_connetion_refused(socket);
                // } else {
                //     if(socket->cb_internal_connection_closed){
                //         socket->cb_internal_connection_closed(socket);
                //     }
                //     if(socket->cb_connection_closed) {
                //         socket->cb_connection_closed(socket);
                //     }
                // }
                break;

            default:
                log_error("Unhandled error in the Event Queue: %s (code: %d), provider specific: %s (code: %d)",
                        fi_strerror(err_entry.err), err_entry.err,
                        fi_eq_strerror(ssocket->ep.eq, err_entry.prov_errno, err_entry.err_data, NULL, 0),
                        err_entry.prov_errno);
        }
        return;

    case -FI_EAGAIN:
    {
        auto fp = &ssocket->ep.eq->fid;
        fi_trywait(m_fabric, &fp, 1);
        break;
    }
    default:
        log_error("Unexpected event %d in send socket Event Queue", event);
        exit(2);
    }
}

void BackendLibfabric::on_recv_socket_cm_event(int fd, void* ptr){
    auto rsocket = reinterpret_cast<recv_socket*>(ptr);
    log_info("recv socket: connection event");
    int ret;
    struct fi_eq_err_entry err_entry;
    uint32_t event = read_cm_event(rsocket->ep.eq, NULL, &err_entry);

    switch (event)
    {
    case FI_CONNECTED:
        log_info("fi_verbs_process_recv_socket_cm_event: FI_CONNECTED");
        if((ret = fi_control(&rsocket->ep.rcq->fid, FI_GETWAIT, &rsocket->ep.cqfd)))
        {
            log_error("Failed to retrieve recv socket Completion Queue wait object: %d, cqfd: %d:", ret, rsocket->ep.cqfd);
        }

        rsocket->cq_ev_ctx = ev_context{rsocket->ep.cqfd, rsocket, std::bind(&BackendLibfabric::on_recv_socket_cq_event, this, std::placeholders::_1, std::placeholders::_2)};
        log_debug("recv_socket: EQ fd %d connected, CQ fd %d", rsocket->ep.eqfd, rsocket->ep.cqfd);
        m_evloop->register_fd(rsocket->cq_ev_ctx);

        log_debug("Adding recv CQ polled fid %d %p", rsocket->ep.cqfd, &rsocket->ep.cq->fid);
        log_debug("recv_socket: EQ fd %d CQ fd %d connected", rsocket->ep.eqfd, rsocket->ep.cqfd);
        if(m_on_connection_established_cb != NULL){
            m_on_connection_established_cb(recv_handle{}, this, m_config.usr_ctx);
        }

        break;

    case FI_SHUTDOWN:
        log_info("fi_verbs_process_recv_socket_cm_event: FI_SHUTDOWN");
        if(m_on_connection_closed_cb != NULL){
            m_on_connection_closed_cb(recv_handle{}, this, m_config.usr_ctx);
        }
        m_rsockets.erase(rsocket->ep.eqfd);
        break;

    case FI_MR_COMPLETE:
    case FI_AV_COMPLETE:
    case FI_JOIN_COMPLETE:
        // Not implemented
        break;

    case -FI_EAGAIN:
    {
        auto fp = &rsocket->ep.eq->fid;
        fi_trywait(m_fabric, &fp, 1);
        break;
    }
    // case -FI_EAVAIL:
    //     log_error("Unhandled error in recv socket EQ code: %d, provider specific code: %d",
    //         err_entry.err, err_entry.prov_errno);
    //     break;

    default:
        log_error("Unexpected event %d in recv socket Event Queue", event);
        exit(2);
        break;
    }
}

void BackendLibfabric::shutdown_send_socket(libfabric_ssocket* ssocket){
    close_endpoint(&ssocket->ep, m_send_domain.domain_ptr);
    m_send_domain.nb_sockets--;
}

void BackendLibfabric::shutdown_listen_socket(libfabric_lsocket* lsocket){

}

void BackendLibfabric::shutdown_recv_socket(recv_socket* rsocket){
    close_endpoint(&rsocket->ep, m_listen_domain.domain_ptr);
}


} //namespace NetioBackend