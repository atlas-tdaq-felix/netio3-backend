#pragma once
#include <memory>
#include <thread>

#include "eventloop.h"
#include "nn-backend.h"

struct usr_ctx {
  unsigned int n;
  unsigned int messages;
  bool is_server;
};

void on_data(const NetioBackend::network_handle& hdl, char* data, int size, NetioBackend::NetworkBackend* backend, void * ctx){
    std::string s;
    std::string data_string(data, size);
    strcmp(data_string.c_str(), "Ping!") ? s = "Ping!" : s = "Pong!";
    usr_ctx* usr = static_cast<usr_ctx*>(ctx);
    usr->n++;
    std::cout << (usr->is_server ? "SERVER:" : "CLIENT:") << " message (" << usr->n << "/" << usr->messages << ") \"" << data_string << "\" received, Recieved: " << data_string << std::endl;
    if (!usr->is_server && (usr->n > usr->messages)) {
       std::cout << "CLIENT: All messages received" << std::endl;
       exit(0); //TODO: no exit here
    }
    usleep(100000);
    auto handle = std::get<NetioBackend::send_handle>(hdl);
    try{
      int res = backend->send_data(s.c_str(), s.length()+1, handle);
      std::cout << "sending successful: " << res << std::endl;
    } catch (const std::exception& e){
      std::cout << e.what() <<std::endl;
    }
}

void on_connection_established(const NetioBackend::network_handle& hdl, NetioBackend::NetworkBackend* backend, void * ctx){
    std::cout << "Ping Pong on connection establisohed" << std::endl;
}

class PingPong{
  private:
    std::unique_ptr<NetioBackend::NetworkBackend> m_net_backend;
    NetioBackend::network_config m_config;
    bool m_server;
    usr_ctx ctx;
    Eventloop m_evloop;
    std::thread m_evloop_thread;

  public:
    PingPong(NetioBackend::network_type networkType, NetioBackend::network_config config, bool is_server, std::string ip, short unsigned int port, unsigned messages=0) : m_config(config),
          m_server(is_server){
      ctx.n = 0;
      ctx.messages = messages;
      ctx.is_server = is_server;
      m_config.on_data_cb = on_data;
      m_config.on_connection_established_cb = on_connection_established;
      m_config.usr_ctx = &ctx;

      m_net_backend = NetioBackend::NetworkBackend::create(networkType, m_config, &m_evloop);
      if(m_server){
        NetioBackend::network_handle handle = NetioBackend::listen_handle{{ip, port}, NetioBackend::connection_parameters(), NULL, NetioBackend::send_handle{}, true};
        m_evloop_thread = std::thread([=](){m_evloop.evloop_run_for(10);});
        m_net_backend->register_handle(&handle);
        int i = 0;
        while(i < 10){
          sleep(1);
          ++i;
        }
      } else {
        NetioBackend::network_handle handle = NetioBackend::send_handle{{ip, port}, {NetioBackend::RDMA, config.buffersize, config.num_buffers, false}, NULL};
        std::string s = "PingPong!";
        m_evloop_thread = std::thread([=](){m_evloop.evloop_run_for(10);});
        m_net_backend->register_handle(&handle);
        
        sleep(1);
        int ret = m_net_backend->send_data(s.c_str(), 9, std::get<NetioBackend::send_handle>(handle));
        std::cout << "Initial send returned: " << ret << " last char " << std::endl;

        std::cout << std::endl;
        int i = 0;
        while(i < 10){
          sleep(1);
          ++i;
        }
      }
    }
    ~PingPong(){
      m_evloop_thread.join();
    }
};
