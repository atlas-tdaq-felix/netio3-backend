#include "ping_pong.h"

#include "docopt/docopt.h"
#include "felixtag.h"

#include <thread>

static const char USAGE[] =
R"(ping-pong - Ping-pong test application (client and server).

    Usage:
      ping-pong [options]

    Options:
      -h, --help                         Show this screen.
          --version                      Show version.
      -c, --client                       Run client
      -s, --server                       Run server
      -i, --ip=IP                        Specify ip address [default: 127.0.0.1]
      -p, --port=PORT                    Specify port [default: 1337]
      -m, --mode=NETWORK_MODE            Specify network mode [default: TCP]
      -t, --type=NETWORK_TYPE            Specify network type [default: POSIX_SOCKETS]
          --messages=MESSAGES            Specify number of messages after which to exit [default: 0]

    Network Mode: TCP, UDP, RDMA, SHMEM
    Network Type: NONE, POSIX_SOCKETS, LIBFABRIC, UCX
)";

int main(int argc, char** argv){
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    try {
      std::string ip = args["--ip"].asString();
      short unsigned int port = args["--port"].asLong();
      bool run_server = args["--server"].asBool();
      bool run_client_and_server = !run_server && !args["--client"].asBool();
      NetioBackend::network_mode mode = NetioBackend::network_mode_from_string(args["--mode"].asString());
      NetioBackend::network_type type = NetioBackend::network_type_from_string(args["--type"].asString());
      unsigned messages = args["--messages"].asLong();


      NetioBackend::network_config config{false, 12, 64 *1024, mode, NULL, NULL, NULL, NULL, NULL};

      if (run_client_and_server) {
        std::thread server([=](){PingPong(type, config, true, ip, port, messages);});
        sleep(1);
        auto client = PingPong(type, config, false, ip, port, messages);
        server.join();
      } else {
        auto client_or_server = PingPong(type, config, run_server, ip, port, messages);
        exit(0);
      }
    } catch (std::invalid_argument const& error) {
      std::cerr << "Argument or option of wrong type" << std::endl;
      std::cerr << error.what() << std::endl;
      std::cout << std::endl;
      std::cout << USAGE << std::endl;
      exit(-1);
    }

}
