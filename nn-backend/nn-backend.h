#pragma once
#include <iostream>
#include <variant>
#include <memory>
#include <vector>
#include <algorithm>
#include <map>
#include <stdexcept>
#include <utility>
#include <sys/eventfd.h>
#include <unistd.h>
#include <rdma/fi_domain.h>
extern "C" {
  #include "log.h"
}
#include <cstring>
#include <mutex>


//#include "nn-utilities.h"
#include "eventloop.h"

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

namespace NetioBackend
{
    class NetworkBackend;
    enum network_mode {TCP, UDP, RDMA, SHMEM};
    enum network_type {NONE, POSIX_SOCKETS, LIBFABRIC, UCX};

    static std::map<std::string, network_mode> network_mode_value = {
        { "TCP", TCP },
        { "UDP", UDP },
        { "RDMA", RDMA },
        { "SHMEM", SHMEM },
    };

    inline static network_mode network_mode_from_string(const std::string& mode) {
        std::map<std::string, network_mode>::iterator i = network_mode_value.find(mode);

        if (i == network_mode_value.end()) {
            throw std::invalid_argument("network_mode: " + mode);
        } else {
            return i->second;
        }
    }

    static std::map<std::string, network_type> network_type_value {
        { "NONE", NONE },
        { "POSIX_SOCKETS", POSIX_SOCKETS },
        { "LIBFABRIC", LIBFABRIC },
        { "UCX", UCX },
    };

    inline static network_type network_type_from_string(const std::string& type) {
        std::map<std::string, network_type>::iterator i = network_type_value.find(type);

        if (i == network_type_value.end()) {
            throw std::invalid_argument("network_type: " + type);
        } else {
            return i->second;
        }
    }

    struct socket_addr{
        std::string ip;
        unsigned short port;
        bool operator<(const socket_addr& rhs) const
        {
            if (port < rhs.port)
            {
            return true;
            }
            return false;
        }
    };

    struct nn_socket{
        int fd;
        nn_socket() : fd(-1){}
        explicit nn_socket(int fd) : fd(fd){}
    };
    struct nn_send_socket: nn_socket{
        nn_send_socket() : nn_socket(){}
        explicit nn_send_socket(int fd) : nn_socket(fd){}
    };

    struct nn_listen_socket: nn_socket{
        nn_listen_socket() : nn_socket(){}
        explicit nn_listen_socket(int fd) : nn_socket(fd){}
    };

    struct connection_parameters{
        network_mode mode = TCP;
        size_t buf_size = 0;
        size_t num_buf = 0;
        bool zero_copy = false;
        char* mr_start = NULL;
    };

    struct domain_ctx;

    struct network_buffer {
        char* buf;
        size_t size;
        size_t pos = 0;
        size_t left_to_read = 0;
        std::vector<char> buf_array;
        fid_mr* mr = NULL; //TODO: Introduce super and subclasses of buffers as well
        domain_ctx* domain = NULL;
        explicit network_buffer(size_t size) : size(size){
            buf_array.resize(size, '\0');
            buf = buf_array.data();
        }
        explicit network_buffer(size_t size, char* buffer) : buf(buffer), size(size){}
        network_buffer(){}; //TODO: remove default constructor
    };

    struct send_handle{
        const socket_addr remote_addr;
        connection_parameters connection_param;
        nn_send_socket* ssocket;
    };

    struct listen_handle{
        const socket_addr local_addr;
        connection_parameters connection_param;
        nn_listen_socket* lsocket;
        send_handle shandle;
        bool bidirectional = false;
    };

    struct recv_handle{
        const socket_addr remote_addr;
    };



    using network_handle = std::variant<std::monostate, send_handle, listen_handle, recv_handle>;

    struct network_config {
        bool zero_copy;
        size_t num_buffers;
        size_t buffersize;
        network_mode mode;
        void* usr_ctx;

        //Callbacks
        std::function<void(const network_handle& info, char* data, int size, NetworkBackend* backend, void* usr_ctx)> on_data_cb;
        std::function<void(const network_handle& info, NetworkBackend* backend, void* usr_ctx)> on_connection_established_cb;
        std::function<void(const network_handle& info, NetworkBackend* backend, void* usr_ctx)> on_connection_closed_cb;
        std::function<void(const network_handle& info, NetworkBackend* backend, void* usr_ctx)> on_connection_refused_cb;
        std::function<void(const network_handle& info, NetworkBackend* backend, void* usr_ctx)> on_send_completed_cb;
    };
    class NetworkBackend{
    public:
        network_config m_config;
        NetworkBackend(const network_config& config) : m_config(config){}
        virtual ~NetworkBackend(){};
        static std::unique_ptr<NetworkBackend> create(network_type type, network_config& config, Eventloop* evloop);
        /**
         * General network configurations should be added to the config struct and passed to the constructor.
         * The follwing four functions are the current suggestion for the iterface of all backends towards nn-netio.
         * The map al the usual functionality to operate the network for all backends.
         * The generic network handle is of type std::variant, which allows to use a different configuration struct for each backend. 
         * If in the end a single stuct is sufficient, the std::variant can eb removed to reduce the small overhead here.  
         * 
         * register_handle should be used to register/initialize all kind of network resource. especially startign listening or establishing a connection.
         * On the contrary, close_handle should close all associated network and memory resources. 
         * Note that the particular backends should hold their own resources and also be in charge for manageing them. Hence, the network handle should be rather used to identify endpoints
         * instead of exposing them to upper layers.
         * 
         * In addition to these four interface functions, there is also a set of callback functions that is defined in the network config and that are triggered for certain actions.
         * 
         */
        virtual void run() = 0; //Since we are passing the eventloop now as a parameter, we might not need this function.
        virtual void register_handle(network_handle* handle) = 0;
        virtual void close_handle(network_handle* handle) = 0;
        virtual size_t send_data(const char* data, size_t size, send_handle& handle, bool data_in_mr = false) = 0;
    };
};