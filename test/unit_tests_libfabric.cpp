#define CATCH_CONFIG_MAIN
#define UNIT_TESTING

#include <catch2/catch_test_macros.hpp>
#include "nn-backend.h"
#include "backend-libfabric.hpp"
#include <string>
#include <thread>



TEST_CASE( "Test init function", "[init_test]" ) {
    NetioBackend::network_config config{false, 64, 1, NetioBackend::TCP, NULL, NULL, NULL, NULL};
    Eventloop evloop;
    std::unique_ptr<NetioBackend::NetworkBackend> net_backend = NetioBackend::NetworkBackend::create(NetioBackend::LIBFABRIC, config, &evloop);

    SECTION("Uninitialized handle") {
        NetioBackend::network_handle non_init_handle;
        REQUIRE_THROWS_AS(net_backend->register_handle(&non_init_handle), std::invalid_argument);
    }
    SECTION("Empty listen_handle") {
        NetioBackend::network_handle empty_lhandle = NetioBackend::listen_handle{{"", 0}, NetioBackend::connection_parameters(), NULL, NetioBackend::send_handle{}};
        REQUIRE_THROWS_AS(net_backend->register_handle(&empty_lhandle), std::invalid_argument);
    }
    SECTION("Empty send_handle") {
        NetioBackend::network_handle empty_shandle = NetioBackend::send_handle{{"", 0}, NetioBackend::connection_parameters(), NULL};
        REQUIRE_THROWS_AS(net_backend->register_handle(&empty_shandle), std::invalid_argument);
    }
    SECTION("Working example") {
        NetioBackend::network_handle working_handle = NetioBackend::listen_handle{{"127.0.0.1", 1337}, NetioBackend::connection_parameters(), NULL, NetioBackend::send_handle{}};
        net_backend->register_handle(&working_handle);
        REQUIRE(static_cast<NetioBackend::libfabric_lsocket*>(std::get<NetioBackend::listen_handle>(working_handle).lsocket)->pep != NULL);
    }
}

TEST_CASE( "Test closing handle", "[close_handle_test]" ) {
    NetioBackend::network_config config{false, 64, 1, NetioBackend::TCP, NULL, NULL, NULL, NULL};
    Eventloop evloop;
    std::unique_ptr<NetioBackend::NetworkBackend> net_backend = NetioBackend::NetworkBackend::create(NetioBackend::LIBFABRIC, config, &evloop);
    NetioBackend::network_handle handle = NetioBackend::listen_handle{{"127.0.0.1", 7331}, NetioBackend::connection_parameters(), NULL,  {{"", 0}, NetioBackend::connection_parameters(), NULL}};

    SECTION("Uninitialized handle") {
        NetioBackend::network_handle non_init_handle;
        REQUIRE_THROWS_AS(net_backend->close_handle(&non_init_handle), std::invalid_argument);
    }
    SECTION("Empty handle") {
        NetioBackend::network_handle empty_handle = NetioBackend::send_handle{{"", 0}, NetioBackend::connection_parameters(), NULL};
        REQUIRE_THROWS_AS(net_backend->close_handle(&empty_handle), std::invalid_argument);
    }
    SECTION("Working example") {
        net_backend->register_handle(&handle);
        REQUIRE(static_cast<NetioBackend::libfabric_lsocket*>(std::get<NetioBackend::listen_handle>(handle).lsocket)->pep != NULL);
        net_backend->close_handle(&handle);
        REQUIRE(std::get<NetioBackend::listen_handle>(handle).lsocket == NULL);
    }
}


TEST_CASE( "Test send TCP", "[send_test_tcp]" ) {
    NetioBackend::network_config config{false, 1, 64, NetioBackend::TCP, NULL, NULL, NULL, NULL};
    Eventloop evloop;
    std::unique_ptr<NetioBackend::NetworkBackend> net_backend = NetioBackend::NetworkBackend::create(NetioBackend::LIBFABRIC, config, &evloop);
    std::string s = "Hello";
    SECTION("Empty handle") {
        auto empty_handle = NetioBackend::send_handle{{"", 0}, NetioBackend::connection_parameters(), NULL};
        REQUIRE_THROWS_AS(net_backend->send_data(s.c_str(), s.length(), empty_handle), std::runtime_error);
    }
    SECTION("Send to non existing remote:") {
        auto handle = NetioBackend::send_handle{{"127.0.0.1", 3333}, {NetioBackend::TCP, config.buffersize, 1, false}, NULL};
        REQUIRE_THROWS_AS(net_backend->send_data(s.c_str(), s.length(), handle), std::runtime_error);
    }
    SECTION("Send to existing remote, no send buffers allocated:") {  
        std::thread server([&](){
            NetioBackend::network_config config{false, 64, 1, NetioBackend::TCP, NULL, NULL, NULL, NULL};
            std::unique_ptr<NetioBackend::NetworkBackend> server_net_backend = NetioBackend::NetworkBackend::create(NetioBackend::LIBFABRIC, config, &evloop);
            NetioBackend::network_handle lhandle = NetioBackend::listen_handle{{"127.0.0.1", 1907}, NetioBackend::connection_parameters(), NULL, {{"", 0}, NetioBackend::connection_parameters(), NULL}};
            server_net_backend->register_handle(&lhandle);
            REQUIRE(static_cast<NetioBackend::libfabric_lsocket*>(std::get<NetioBackend::listen_handle>(lhandle).lsocket)->pep != NULL);
            sleep(2);
        });
        sleep(1);
        NetioBackend::network_handle  handle = NetioBackend::send_handle{{"127.0.0.1", 1907}, NetioBackend::connection_parameters{NetioBackend::TCP, config.buffersize, 0, false, NULL}, NULL};
        net_backend->register_handle(&handle);
        REQUIRE(static_cast<NetioBackend::libfabric_ssocket*>(std::get<NetioBackend::send_handle>(handle).ssocket)->ep.ep != NULL);
        REQUIRE_THROWS_AS(net_backend->send_data(s.c_str(), s.length(), std::get<NetioBackend::send_handle>(handle)), std::runtime_error);
        server.join();
        net_backend->close_handle(&handle);
        REQUIRE(static_cast<NetioBackend::libfabric_ssocket*>(std::get<NetioBackend::send_handle>(handle).ssocket) == NULL);
    }
    SECTION("Send to existing remote:") {   
        std::thread ev_thread([&](){evloop.evloop_run_for(5);});
        std::thread server([&](){
            NetioBackend::network_config config{false, 1, 64, NetioBackend::TCP, NULL, NULL, NULL, NULL, NULL, NULL};
            std::unique_ptr<NetioBackend::NetworkBackend> server_net_backend = NetioBackend::NetworkBackend::create(NetioBackend::LIBFABRIC, config, &evloop);
            NetioBackend::network_handle lhandle = NetioBackend::listen_handle{{"127.0.0.1", 2023}, NetioBackend::connection_parameters{NetioBackend::TCP, config.buffersize, 1, false, NULL}, NULL, {{"", 0}, NetioBackend::connection_parameters(), NULL}};
            server_net_backend->register_handle(&lhandle);
            REQUIRE(static_cast<NetioBackend::libfabric_lsocket*>(std::get<NetioBackend::listen_handle>(lhandle).lsocket)->pep != NULL);
            sleep(3);
        });
        NetioBackend::network_handle  handle = NetioBackend::send_handle{{"127.0.0.1", 2023}, NetioBackend::connection_parameters{NetioBackend::TCP, config.buffersize, 1, false, NULL}, NULL};
        net_backend->register_handle(&handle);
        REQUIRE(static_cast<NetioBackend::libfabric_ssocket*>(std::get<NetioBackend::send_handle>(handle).ssocket)->ep.ep != NULL);
        sleep(1); //To do it properly, we need a callback that waits for the connection to be established
        REQUIRE(net_backend->send_data(s.c_str(), s.length(), std::get<NetioBackend::send_handle>(handle)) == 0);
        server.join();
        ev_thread.join();
    }
    SECTION("Use previously allocated MR:") {   
        std::thread ev_thread([&](){evloop.evloop_run_for(5);});
        std::thread server([&](){
            NetioBackend::network_config config{false, 1, 64, NetioBackend::TCP, NULL, NULL, NULL, NULL, NULL, NULL};
            std::unique_ptr<NetioBackend::NetworkBackend> server_net_backend = NetioBackend::NetworkBackend::create(NetioBackend::LIBFABRIC, config, &evloop);
            NetioBackend::network_handle lhandle = NetioBackend::listen_handle{{"127.0.0.1", 2345}, NetioBackend::connection_parameters{NetioBackend::TCP, config.buffersize, 1, false, NULL}, NULL, {{"", 0}, NetioBackend::connection_parameters(), NULL}};
            server_net_backend->register_handle(&lhandle);
            REQUIRE(static_cast<NetioBackend::libfabric_lsocket*>(std::get<NetioBackend::listen_handle>(lhandle).lsocket)->pep != NULL);
            sleep(3);
        });
        NetioBackend::network_handle  handle = NetioBackend::send_handle{{"127.0.0.1", 2345}, NetioBackend::connection_parameters{NetioBackend::TCP, config.buffersize, 1, false, s.data()}, NULL};
        net_backend->register_handle(&handle);
        REQUIRE(static_cast<NetioBackend::libfabric_ssocket*>(std::get<NetioBackend::send_handle>(handle).ssocket)->ep.ep != NULL);
        sleep(1); //To do it properly, we need a callback that waits for the connection to be established
        REQUIRE(net_backend->send_data(s.c_str(), s.length(), std::get<NetioBackend::send_handle>(handle), true) == 0);
        server.join();
        ev_thread.join();
    }
}