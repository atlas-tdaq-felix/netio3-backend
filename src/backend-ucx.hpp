#include "nn-backend.h"


namespace NetioBackend {

    class BackendUCX: public NetworkBackend {
    public:
        explicit BackendUCX(network_config& config, Eventloop* evloop);
        ~BackendUCX();
        void run() override;
        void register_handle(network_handle* handle) override;
        void close_handle(network_handle* handle) override;
        size_t send_data(const char* data, size_t size, send_handle& handle, bool data_in_mr = false) override;
    private:
        //void establish_oob_connection(network_handle* handle);
        Eventloop* m_evloop;
    };
};