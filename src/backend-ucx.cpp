#include "backend-ucx.hpp"


namespace NetioBackend{

BackendUCX::BackendUCX(network_config& config, Eventloop* evloop) : NetworkBackend(config),
                                                                    m_evloop(evloop){
    /* UCP temporary vars */
    // ucp_params_t ucp_params;
    // ucp_worker_params_t worker_params;
    // ucp_config_t* uconfig;
    // ucs_status_t status;

    // /* UCP handler objects */
    // ucp_context_h ucp_context;
    // ucp_worker_h ucp_worker;

    // status = ucp_config_read(NULL, NULL, &uconfig);

    // ucp_params.field_mask   = UCP_PARAM_FIELD_FEATURES |
    //                           UCP_PARAM_FIELD_REQUEST_SIZE |
    //                           UCP_PARAM_FIELD_REQUEST_INIT;
    // ucp_params.features     = UCP_FEATURE_TAG;

    // status = ucp_init(&ucp_params, uconfig, &ucp_context);

    // ucp_config_print(uconfig, stdout, NULL, UCS_CONFIG_PRINT_CONFIG);
}
                                            
BackendUCX::~BackendUCX(){
    
}

// void BackendUCX::establish_oob_connection(network_handle* handle){

// }






void BackendUCX::run(){}
void BackendUCX::register_handle(network_handle* handle){}
void BackendUCX::close_handle(network_handle* handle){}
size_t BackendUCX::send_data(const char* data, size_t size, send_handle& handle, bool data_in_mr){return 0;}


} //namespace NetioBackend