#include "backend-libfabric.hpp"

namespace NetioBackend{
void BackendLibfabric::on_send_socket_cq_event(int fd, void* ptr){
    log_trace("Send socket CQ event");
    auto ssocket = reinterpret_cast<libfabric_ssocket*>(ptr);
    struct fi_cq_data_entry completion_entry[MAX_CQ_EVENTS]; //TODO: use cpp style
    int ret = fi_cq_read(ssocket->ep.cq, &completion_entry, ssocket->ep.cq_size);
    log_trace("send socket fd %d: %d completion events", fd, ret);
    if(ret < 0){
        if(ret == -FI_EAGAIN){ //If no completions are available to return from the CQ, -FI_EAGAIN will be returned.
            struct fid* fp = &ssocket->ep.cq->fid;
            fi_trywait(m_fabric, &fp, 1);
            return;
        }
        else if(ret == -FI_EAVAIL)
        {
            fi_cq_err_entry err_entry;
            if((ret = fi_cq_readerr(ssocket->ep.cq, &err_entry, 0)) < 0)
            {
                log_error("Failed to retrieve details on Completion Queue error of send socket, error %d: %s", ret, fi_strerror(-ret));
            }
            log_error("Completion Queue read error %d of send socket: %s", err_entry.err, fi_strerror(err_entry.err));
            log_error("Provider-specific error %d: %s", err_entry.prov_errno, fi_cq_strerror(ssocket->ep.cq, err_entry.prov_errno, err_entry.err_data, NULL, 0));
            if(err_entry.err == FI_EIO) {
                // I/O error, the CM event code can handle this
                log_error("Send socket CQ I/O error, connection possibly closed: ignored");
                return;
            }
            if(err_entry.err == FI_ECANCELED) {
                // Operation Cancelled
                log_error("Send socket CQ operation cancelled.");
                return;
            }
            log_error("Send socket Completion Queue unhandled specific read error %d: %s", err_entry.err, fi_strerror(err_entry.err));
        }
        else{
            log_error("Send socket Completion Queue unhandled read error %d: %s", ret, fi_strerror(-ret));
        }
    } else {
        for(int i=0; i < ret; ++i){
            auto key = reinterpret_cast<uint64_t>(completion_entry[i].op_context);
            log_debug("Send completed. Key was %lu", key);
            ssocket->available_buffers.push(key);
            if(m_on_send_completed_cb != NULL){
                m_on_send_completed_cb(send_handle{{ssocket->addr.ip, ssocket->addr.port}, ssocket->conn_parameters, ssocket}, this, m_config.usr_ctx);
            }
        }
    }
}

void BackendLibfabric::on_recv_socket_cq_event(int fd, void* ptr){
    log_trace("Receive socket CQ event");
    auto rsocket = reinterpret_cast<recv_socket*>(ptr);
    struct fi_cq_data_entry completion_entry[MAX_CQ_EVENTS]; //use cpp style
    int ret = fi_cq_read(rsocket->ep.rcq, &completion_entry, rsocket->ep.cq_size);
    log_trace("recv socket fd %d: %d completion events", fd, ret);

    if(ret < 0)
    {
        if(ret == -FI_EAGAIN){
            fid* fp = &rsocket->ep.rcq->fid;
            fi_trywait(m_fabric, &fp, 1);
            return;
        }
        else if(ret == -FI_EAVAIL){
            int r;
            struct fi_cq_err_entry err_entry;
            if((r = fi_cq_readerr(rsocket->ep.cq, &err_entry, 0)) < 0)
            {
                log_error("Failed to retrieve details on Completion Queue error of recv socket, error %d: %s", r, fi_strerror(-r));
            }
            log_error("code %d reading Completion Queue of recv socket: %s", err_entry.err, fi_strerror(err_entry.err));
            log_error("Provider-specific error %d: %s", err_entry.prov_errno, fi_cq_strerror(rsocket->ep.cq, err_entry.prov_errno, err_entry.err_data, NULL, 0));
            if(err_entry.err == FI_EIO) {
                // I/O error, the CM event code can handle this
                log_debug("Send socket CQ I/O error, connection possibly closed: ignored");
                return;
            }
            if(err_entry.err == FI_ECANCELED) {
                // Operation Cancelled
                log_debug("Send socket CQ operation cancelled.");
                return;
            }
        }
        else{
            log_error("Recv socket unhandled Completion Queue error %d: %s", ret, fi_strerror(-ret));
        }
    }
    else{
        for(int i = 0; i < ret; ++i){
        auto buffer = static_cast<network_buffer*>(completion_entry[i].op_context);
        char* data = buffer->buf;
        size_t size = completion_entry[i].len;
        if(completion_entry[i].flags & FI_REMOTE_CQ_DATA) {
            log_debug("Completion has remote CQ data");
            uint64_t imm = completion_entry[i].data;
            log_debug("recv completion immediate data: 0x%lu", imm);
            // if(socket->lsocket->cb_msg_imm_received) {
            // socket->lsocket->cb_msg_imm_received(socket, buffer, data, size, imm);
            // } else if(socket->lsocket->cb_msg_received) {
            // socket->lsocket->cb_msg_received(socket, buffer, data, size);
            // }
            if(m_on_data_cb != NULL){
                m_on_data_cb(recv_handle{}, data, size, this, m_config.usr_ctx);
            }
        } else {
            log_debug("Completion has NO remote CQ data, size is: %d", size);
            /**
             * we receive data by reading the op_context field of the completion entry. When we post the buffer, we specify the context is a 
             * network_buffer struct and the buf and size field are the addresses for the data and size of the iov vector.
             */
            if(m_on_data_cb != NULL){
                m_on_data_cb(recv_handle{}, data, size, this, m_config.usr_ctx);
            }

        }
        // FLX-1194, posting buffers to be done by USER !! TODO: check how to handle this now in netio3. For testing reasons, I am adding it again to post the buffer right away
        post_buffers(buffer, &rsocket->ep);
        }
    }

}

}