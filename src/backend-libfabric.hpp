#include <rdma/fabric.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <arpa/inet.h>
#include <queue>

#include "nn-backend.h"

#define MAX_CQ_ENTRIES (4096)
#define MAX_CQ_EVENTS (10)
#define DOMAIN_MAX_MR (1024)

namespace NetioBackend{

    struct endpoint{
        struct fi_info* fi = NULL;
        struct fid_eq *eq = NULL;
        struct fid_ep *ep = NULL;
        struct fid_cq* cq = NULL;
        struct fid_cq* rcq = NULL;
        int eqfd = -1;
        int cqfd = -1;
        size_t cq_size = MAX_CQ_EVENTS;
        endpoint(){}
        ~endpoint(){
            if (fi != NULL){
                fi_freeinfo(fi);
            }
        }
    };




    struct domain_ctx{
        struct fid_domain* domain_ptr = NULL; //struct will be created by libfarbic. 

        uint64_t req_key = 0;
        uint32_t reg_mr = 0;
        std::vector<fid> mr;

        int nb_sockets = 0;
    };


    struct libfabric_ssocket;
    struct libfabric_lsocket;
    struct recv_socket;

    class BackendLibfabric: public NetworkBackend{
    public:
        explicit BackendLibfabric(network_config& config, Eventloop* evloop);
        ~BackendLibfabric();
        void run() override; 
        void register_handle(network_handle* handle) override;
        void close_handle(network_handle* handle) override;
        size_t send_data(const char* data, size_t size, send_handle& handle, bool data_in_mr = false) override;

        friend class recv_socket;
        friend class libfabric_ssocket;
        friend class libfabric_lsocket;
    private:
        Eventloop* m_evloop;
        fid_fabric* m_fabric;
        domain_ctx m_send_domain;
        domain_ctx m_listen_domain;
        fi_info* m_info;
        void setupDomains(const char* host, const char* port, uint64_t flags);
        void setProvider(fi_fabric_attr* attr);
        void init_listen(listen_handle* connection);
        void init_send(send_handle* connection, fi_info* info = NULL);
        void register_buffer(network_buffer* buf, int access_flag);
        void post_buffers(network_buffer* buf, endpoint* ep);
        void on_listen_socket_cm_event(int,void*);
        void on_send_socket_cm_event(int,void*);
        void on_recv_socket_cm_event(int,void*);
        void on_send_socket_cq_event(int,void*);
        void on_recv_socket_cq_event(int,void*);
        int read_cm_event(struct fid_eq* eq, struct fi_info** info, struct fi_eq_err_entry* err_entry);
        int handle_connreq(libfabric_lsocket* lsocket, fi_info* info);
        int connect_endpoint(endpoint* ep, fid_domain* domain, fi_info* info);
        int close_endpoint(endpoint* ep, fid_domain* domain);
        void shutdown_send_socket(libfabric_ssocket* ssocket);
        void shutdown_listen_socket(libfabric_lsocket* lsocket);
        void shutdown_recv_socket(recv_socket* rsocket);
        std::map<socket_addr, libfabric_lsocket> m_lsockets;
        std::map<socket_addr, libfabric_ssocket> m_ssockets;
        std::map<int, recv_socket> m_rsockets;  

        std::function<void(const network_handle& info, char* data, int size, NetworkBackend* backend, void* usr_ctx)> m_on_data_cb;
        std::function<void(const network_handle& info,NetworkBackend* backend, void* usr_ctx)> m_on_connection_established_cb;
        std::function<void(const network_handle& info,NetworkBackend* backend, void* usr_ctx)> m_on_connection_closed_cb;
        std::function<void(const network_handle& info,NetworkBackend* backend, void* usr_ctx)> m_on_connection_refused_cb;
        std::function<void(const network_handle& info,NetworkBackend* backend, void* usr_ctx)> m_on_send_completed_cb;
    };

    struct recv_socket{
        endpoint ep;
        ev_context eq_ev_ctx;
        ev_context cq_ev_ctx;
        std::vector<network_buffer> buffers;
        BackendLibfabric* backend;
        send_handle shandle;

        void init_buffers(uint64_t num_buf, uint64_t buf_size){
            uint64_t i = 0;
            while(i++ < num_buf){
                buffers.emplace_back(buf_size);
                backend->register_buffer(&buffers.back(), FI_RECV);
            }
        }                                                                                               
        explicit recv_socket(BackendLibfabric* backend) :   ep(endpoint()),
                                                            backend(backend),
                                                            shandle(send_handle{}){}
        recv_socket(recv_socket&& source):  ep(source.ep),
                                            backend(source.backend),
                                            shandle(source.shandle){
            source.ep = endpoint();
            source.backend = NULL;
            //source.shandle = send_handle{}; TODO
        }
        
        ~recv_socket(){
            if(backend != NULL){
                for(auto& buf : buffers){
                    fi_close(&buf.mr->fid);
                    buf.domain->reg_mr--;
                }
                backend->shutdown_recv_socket(this);
            }
        } 
    };

    struct libfabric_lsocket: public nn_listen_socket{
        ev_context eq_ev_ctx;
        endpoint ep;
        fid_pep *pep = NULL;
        connection_parameters conn_parameters;
        BackendLibfabric* m_backend;

        explicit libfabric_lsocket(listen_handle& handle, BackendLibfabric* backend) : ep(endpoint()),
                                                            conn_parameters(handle.connection_param),
                                                            m_backend(backend){}
        ~libfabric_lsocket(){
            m_backend->shutdown_listen_socket(this);
        }
    };

    struct libfabric_ssocket: public nn_send_socket{
        socket_addr addr;
        endpoint ep;
        ev_context eq_ev_ctx;
        ev_context cq_ev_ctx;
        connection_parameters conn_parameters;
        BackendLibfabric* m_backend;
        std::vector<network_buffer> buffers;
        std::queue<uint64_t> available_buffers;

        void init_buffers(){
            if(conn_parameters.mr_start == NULL){
                uint64_t i = 0;
                while(i++ < conn_parameters.num_buf){
                    buffers.emplace_back(conn_parameters.buf_size);
                    m_backend->register_buffer(&buffers.back(), FI_SEND);
                    available_buffers.push(available_buffers.size());
                } 
            } else {
                //TODO: use already existing MR and register it.
                if(conn_parameters.num_buf > 1){
                    log_warn("Trying to add more than one buffer but passed address of existing one.");
                }
                buffers.emplace_back(conn_parameters.buf_size, conn_parameters.mr_start);
                m_backend->register_buffer(&buffers.back(), FI_SEND);
                available_buffers.push(available_buffers.size());
                log_warn("Trying to register already allocated buffer. Not implemented yet!");
                 
            }
        }   
        explicit libfabric_ssocket(send_handle& handle, BackendLibfabric* backend) : addr(socket_addr()),
                                                            ep(endpoint()),
                                                            conn_parameters(handle.connection_param),
                                                            m_backend(backend){}
        ~libfabric_ssocket(){
            log_info("Send socket destructor for %p", this);
            int i = 0;
            for(auto& buf : buffers){
                log_debug("Send socket destructor: Closing memory region #%d, %d remaining.", i++, (buf.domain->reg_mr - 1));
                fi_close(&buf.mr->fid);
                buf.domain->reg_mr--;
            }
            m_backend->shutdown_send_socket(this);
        } 
    };

};


