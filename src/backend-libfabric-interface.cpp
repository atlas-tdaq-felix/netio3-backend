#include "backend-libfabric.hpp"

namespace NetioBackend{
BackendLibfabric::BackendLibfabric(network_config& config, Eventloop* evloop) : NetworkBackend(config),
                                                             m_evloop(evloop),
                                                             m_fabric(NULL),
                                                             m_send_domain(domain_ctx()),
                                                             m_listen_domain(domain_ctx()),
                                                             m_info(NULL),
                                                             m_on_data_cb(config.on_data_cb),
                                                             m_on_connection_established_cb(config.on_connection_established_cb),
                                                             m_on_connection_closed_cb(config.on_connection_closed_cb),
                                                             m_on_connection_refused_cb(config.on_connection_refused_cb),
                                                             m_on_send_completed_cb(config.on_send_completed_cb)
{
    log_set_level(3);
}

BackendLibfabric::~BackendLibfabric(){
    if(m_info != NULL){
        fi_freeinfo(m_info);
    }
    m_lsockets.clear();
    m_ssockets.clear();
    m_rsockets.clear();

    int ret = 0;
    if (m_send_domain.domain_ptr != NULL && (ret = fi_close(&m_send_domain.domain_ptr->fid))) {
        log_error("Failed to close send domain %d: %s", ret, fi_strerror(-ret));
    }
    if (m_listen_domain.domain_ptr != NULL && (ret = fi_close(&m_listen_domain.domain_ptr->fid))) {
        log_error("Failed to close listen domain %d: %s", ret, fi_strerror(-ret));
    }
    // Closing the fabric seems to fail all the time, so I removed it for now.
    // if (m_fabric != NULL && (ret = fi_close(&m_fabric->fid))) {
    //     log_error("Failed to close fabric %d: %s", ret, fi_strerror(-ret));
    // }
}

void BackendLibfabric::run(){
    m_evloop->evloop_run();
}


void BackendLibfabric::register_handle(network_handle* handle){
    std::visit(
    overloaded{[=](listen_handle& handle) {
            if(handle.local_addr.ip != ""){
                if(m_lsockets.count(handle.local_addr) > 0 ){
                    log_info("Listen socket for local add %s:%u is already exists.", handle.local_addr.ip.c_str(), handle.local_addr.port);
                } else {
                    if (m_listen_domain.domain_ptr == NULL){
                        log_debug("Initializing fabric and domain for addr: %s:%u",handle.local_addr.ip.c_str(), handle.local_addr.port);
                        char port_addr[32];
                        snprintf(port_addr, 32, "%u", handle.local_addr.port);
                        try{
                            setupDomains(handle.local_addr.ip.c_str(), port_addr, FI_SOURCE);
                        } catch (const std::exception& e){
                            throw;
                        }
                    }
                    m_lsockets.try_emplace(handle.local_addr, handle, this);
                    handle.lsocket = &m_lsockets.at(handle.local_addr);
                    try{
                        init_listen(&handle);
                    } catch (const std::exception& e){
                            throw;
                    }
                }
            } else {
                throw std::invalid_argument("listen handle does not contain IP.");
            }
        },
        [=](send_handle& handle) {
            if (handle.remote_addr.ip != ""){
                if(m_ssockets.count(handle.remote_addr) > 0 ){
                    log_info("Send socket for remote add %s:%u is already exists.", handle.remote_addr.ip.c_str(), handle.remote_addr.port);
                } else {
                    if (m_send_domain.domain_ptr == NULL){
                        log_debug("Initializing fabric and domain for addr: %s:%u",handle.remote_addr.ip.c_str(), handle.remote_addr.port);
                        char port_addr[32];
                        snprintf(port_addr, 32, "%u", handle.remote_addr.port);
                        try{
                            setupDomains(handle.remote_addr.ip.c_str(), port_addr, 0);
                        } catch (const std::exception& e){
                            throw;
                        }
                    }
                    m_ssockets.try_emplace(handle.remote_addr, handle, this);
                    handle.ssocket = &m_ssockets.at(handle.remote_addr);
                    m_ssockets.at(handle.remote_addr).addr = handle.remote_addr;
                    m_ssockets.at(handle.remote_addr).init_buffers();
                    init_send(&handle);
                }
            } else {
                throw std::invalid_argument("send handle does not contain remote IP.");
            }
        },
        [](std::monostate& handle) {
            throw std::invalid_argument("Network handle is not initialized");
        },
        [](auto& handle) {
            throw std::invalid_argument("Wrong handle for this network provider. Expected listen or send handle.");
        },
    }, *handle);
}

void BackendLibfabric::close_handle(network_handle* handle){
    std::visit(
    overloaded{[=](listen_handle& handle) {
            if(m_lsockets.erase(handle.local_addr) == 0){
                throw std::invalid_argument("Listen socket for given address does not exist.");
            }
            handle.lsocket = NULL;
        },
        [=](send_handle& handle) {
            if (m_ssockets.count(handle.remote_addr) == 0){
                 throw std::invalid_argument("Send socket for given remote address does not exist.");
            }
            if(m_on_connection_closed_cb != NULL){
                m_on_connection_closed_cb(handle, this, m_config.usr_ctx);
            }
            m_ssockets.erase(handle.remote_addr);
            handle.ssocket = NULL;
        },
        [](std::monostate& handle) {
            throw std::invalid_argument("Network handle is not initialized");
        },
        [](auto& handle) {
            throw std::invalid_argument("Wrong handle for this network provider. Expected listen or send handle.");
        },
    }, *handle);
}



size_t BackendLibfabric::send_data(const char* data, size_t size, send_handle& handle, bool data_in_mr){
    int ret=0;
    uint64_t flags;
    if(m_ssockets.count(handle.remote_addr) == 0){
        throw std::runtime_error("No socket for remote address. Have you establsihed the connection?");
    } 
    auto ssocket = &m_ssockets.at(handle.remote_addr);
    if(ssocket->buffers.empty()){
        throw std::runtime_error("No send buffers allocated. Cannot send message.");
    }
    
    if(ssocket->available_buffers.empty()){
        return -FI_EAGAIN;
    }
    auto desc = fi_mr_desc(ssocket->buffers.at(ssocket->available_buffers.front()).mr);
    uint64_t key = ssocket->available_buffers.front();
    if(!data_in_mr){
        if (size > 0 && size <= ssocket->buffers.at(ssocket->available_buffers.front()).size){
            std::memcpy(ssocket->buffers.at(ssocket->available_buffers.front()).buf, data, size);
        } else {
            log_error("Trying to send either zero-sized message or mesage that is larger then allocated buffer. Size was ", size);    
            return -2;
        }
    } else {
        if (size == 0){
            log_error("Trying to send zero-sized message. Size was ", size);  
            return -2;
        }
    }

    iovec iov;
    iov.iov_base = ssocket->buffers.at(ssocket->available_buffers.front()).buf;
    iov.iov_len = size;
    ssocket->available_buffers.pop();
    fi_msg msg;
    msg.msg_iov = &iov; /* scatter-gather array */
    msg.desc = &desc;
    msg.iov_count = 1;
    msg.addr = 0;
    msg.context = reinterpret_cast<void*>(key);
    msg.data = 0;

    log_debug("sending iov message with key 0x%lu", msg.context);

    flags = FI_INJECT_COMPLETE;// | FI_INJECT;

    if(ssocket->ep.ep == NULL || ssocket->ep.ep->msg == NULL){
        log_error("Failed sending message because of null message or null endpoint.");
        return -2;
    }

    if((ret = fi_sendmsg(ssocket->ep.ep, &msg, flags)) != 0)
    {
        if(ret == -FI_EAGAIN) {
            return -1;
        }
        log_error("Failed to send message error (IOV count %lu, key %lu) %d: %s.", 1, key, ret, fi_strerror(-ret));
    }
    log_trace("Send completed with result: %d", ret);    
    return ret;
}

}